﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PodcastManagement.Auth
{
    public partial class frmRegistration : Form
    {
        Form frm = new MainForm();

        public frmRegistration(Form f)
        {
            InitializeComponent();
            frm = f;       
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Close();

            //Utility.OpenForm((Form)Activator.CreateInstance(Assembly.GetExecutingAssembly().GetTypes().Where(a => a.BaseType == typeof(Form) && a.Name == frm.Name).FirstOrDefault()), this);
        }

        private void buttonRegistration_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBoxLogin.Text))
            {
                MessageBox.Show(null, "Введите логин", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (string.IsNullOrEmpty(textBoxPassword.Text))
            {
                MessageBox.Show(null, "Введите пароль", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (string.IsNullOrEmpty(textBoxRePassword.Text))
            {
                MessageBox.Show(null, "Повторите пароль", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (textBoxPassword.Text != textBoxRePassword.Text)
            {
                MessageBox.Show(null, "Пароли не совпадают", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (string.IsNullOrEmpty(textBoxEmail.Text))
            {
                MessageBox.Show(null, "Введите Email", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if(!new Regex(@"^[0-9A-Za-z_-]+@[0-9A-Z_a-z]+\.[A-Za-z]+$").IsMatch(textBoxEmail.Text))
            {
                MessageBox.Show(null, "Email введен некорректно", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            User u = new User();
            u.UserLogin = textBoxLogin.Text;
            u.UserPassword = textBoxPassword.Text;
            u.UserRole = "Читатель";
            u.UserRegisterDate = DateTime.Now;
            u.UserNotify = 0;
            u.UserEmail = textBoxEmail.Text;

            if (pictureBox.Image != null)
            {
                using (var ms = new MemoryStream())
                {
                    pictureBox.Image.Save(ms, ImageFormat.Png);

                    ms.Position = 0;

                    u.UserAvatar = ms.ToArray();
                }
            }



            try
            {
                if(Utility.dBase.User.SingleOrDefault(us => us.UserLogin == u.UserLogin) != null)
                {
                    MessageBox.Show(null, "Данный пользователь существует", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                Utility.dBase.User.Add(u);
                Utility.dBase.SaveChanges();

                MessageBox.Show(null, "Вы успешно зарегистрировались", "Успех", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);

                this.Close();
                //Utility.OpenForm(new MainForm(), this);
            }
            catch
            {
                MessageBox.Show(null, "Произошла ошибка при добавлении", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonLoadFromFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "*.png;*.jpg;*.jpeg;*.gif;*.bmp|*.png;*.jpg;*.jpeg;*.gif;*.bmp";

            if(ofd.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    var img = Bitmap.FromFile(ofd.FileName);
                    pictureBox.Image = img;
                    pictureBox.BackgroundImage = null;
                }
                catch { }
            }
        }

        private void buttonLoadFromCam_Click(object sender, EventArgs e)
        {
            frmGetPicFromCamera frm = new frmGetPicFromCamera();

            frm.FormClosing += delegate
            {
                if (frm.bmp != null)
                {
                    pictureBox.Image = frm.bmp;
                    pictureBox.BackgroundImage = null;
                }
            };

            frm.ShowDialog();
        }
    }
}
