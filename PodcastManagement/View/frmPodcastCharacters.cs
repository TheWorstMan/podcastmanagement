﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PodcastManagement.View
{
    public partial class frmEpisodeCharacters : Form
    {
        private Type backForm;
        private IQueryable<Character> characters;
        private Podcast podcast;


        public frmEpisodeCharacters(Podcast pod, Type backForm = null)
        {
            InitializeComponent();

            this.podcast = pod;

            this.backForm = backForm;

            PictureBox pb = new PictureBox();

            Bitmap bmp;

            using (var mem = new MemoryStream(pod.PodcastLogo))
                bmp = new Bitmap(mem);

            pb.Image = bmp;
            pb.SizeMode = PictureBoxSizeMode.Zoom;
            pb.Width = 200;
            pb.Height = 180;
            pb.Margin = new Padding(40, 0, 0, 0);

            flowLayoutPanel_podcast.Controls.Add(pb);

            listBox_characters.DisplayMember = "CharacterFullName";
            listBox_episodes.DisplayMember = "EpisodeFullName";

            listBox_characters.Items.Clear();

            characters = Utility.dBase.Character.Where(ch => ch.Episode.Any(ep => ep.Season.Podcast.PodcastId == pod.PodcastId));

            foreach (var c in characters)
            {
                listBox_characters.Items.Add(c);                
            }
        }

        private void button_back_Click(object sender, EventArgs e)
        {
            if(backForm == null)
            {
                Utility.OpenForm(new MainForm(), this);
                return;
            }

            Utility.OpenForm((Form)Activator.CreateInstance(backForm), this);
        }

        private void listBox_characters_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(listBox_characters.SelectedItem != null)
            {
                listBox_episodes.Items.Clear();

                var character = listBox_characters.SelectedItem as Character;

                foreach (var ep in character.Episode)
                    listBox_episodes.Items.Add(ep);               
            }
        }

        private void button_search_Click(object sender, EventArgs e)
        {
            listBox_characters.Items.Clear();
            listBox_episodes.Items.Clear();
            listBox_characters.Items.AddRange(characters.Where(c => c.CharacterNameEnglish.Contains(textBox_find.Text) || c.CharacterNameRussian.Contains(textBox_find.Text)).ToArray());

            if (listBox_characters.Items.Count == 0)
                MessageBox.Show(null, "Персонажи не найдены", "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void listBox_episodes_DoubleClick(object sender, EventArgs e)
        {
            if(listBox_episodes.SelectedItem != null)
            {
                var ep = listBox_episodes.SelectedItem as Episode;

                Utility.OpenForm(new View.frmEpisodes(podcast, ep, typeof(frmNavigation)), this);
            }
        }
    }
}
