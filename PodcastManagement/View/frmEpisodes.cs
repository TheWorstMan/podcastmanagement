﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PodcastManagement.View
{
    public partial class frmEpisodes : Form
    {
        private EpisodeNode selectedNode;
        private bool langRu = false;
        private Type backForm;

        class EpisodeNode : TreeNode
        {
            public Episode episode;

            public EpisodeNode(Episode ep)
            {
                this.episode = ep;
                this.Text = string.Format("{0} - {1}", ep.EpisodeNumber, ep.EpisodeRussianName);
            }
        }

        public frmEpisodes(Podcast pc, Episode ep = null, Type backForm = null)
        {
            InitializeComponent();

            TreeNode selNode = null;

            foreach(var s in pc.Season)
            {
                TreeNode season = new TreeNode(s.SeasonName);

                foreach (var e in s.Episode)
                {
                    EpisodeNode epNode = new EpisodeNode(e);

                    season.Nodes.Add(epNode);

                    if (e == ep)
                        selNode = epNode;    
                }

                treeView.Nodes.Add(season);
            }

            if (selNode != null)
            {
                treeView.SelectedNode = selNode;
                treeView_NodeMouseClick(this, new TreeNodeMouseClickEventArgs(selNode, MouseButtons.Left, 1, 0, 0));
            }

            this.backForm = backForm;
        }

        private void treeView_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.Level > 0 && e.Node is EpisodeNode)
            {
                EpisodeNode en = e.Node as EpisodeNode;
                selectedNode = en;

                if (langRu)
                {
                    button_episode_trans.Text = "RU";
                    groupBox_episode.Text = en.episode.EpisodeEnglishName;
                    richTextBox_episodeText.Text = en.episode.Translation.TranslationEnglish ?? "Translation Not Found";
                }
                else
                {
                    button_episode_trans.Text = "ENG";
                    groupBox_episode.Text = selectedNode.episode.EpisodeRussianName;
                    richTextBox_episodeText.Text = selectedNode.episode.Translation.TranslationRussian ?? "Перевода пока нет, но вы держитесь :) \n P.S. Он в процессе";
                }

                if (Utility.current_user != null && Utility.current_user.Episode.Any(ep => ep == en.episode))
                {
                    button_listened.Image = Properties.Resources.eye;
                }
                else
                    button_listened.Image = Properties.Resources.eye2;
            }
        }

        private void frmEpisodes_Load(object sender, EventArgs e)
        {

        }

        private void button_episode_trans_Click(object sender, EventArgs e)
        {
            if(selectedNode != null)
            {
                langRu = !langRu;

                if (langRu)
                {
                    button_episode_trans.Text = "RU";
                    groupBox_episode.Text = selectedNode.episode.EpisodeEnglishName;
                    richTextBox_episodeText.Text = selectedNode.episode.Translation.TranslationEnglish;
                }
                else
                {
                    button_episode_trans.Text = "ENG";
                    groupBox_episode.Text = selectedNode.episode.EpisodeRussianName;
                    richTextBox_episodeText.Text = selectedNode.episode.Translation.TranslationRussian ?? "";
                }
            }
        }

        private void richTextBox_episodeText_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            if (backForm == null)
            {
                Utility.OpenForm(new MainForm(), this);
                return;
            }

            Utility.OpenForm((Form)Activator.CreateInstance(backForm), this);
        }

        private void button_listened_Click(object sender, EventArgs e)
        {
            if(Utility.current_user == null)
            {
                MessageBox.Show(null, "Для этого действия вы должны авторизоваться", "Ошибка авторизации", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if(Utility.current_user.Episode.Any(ep => ep == selectedNode.episode))
            {
                Utility.current_user.Episode.Remove(selectedNode.episode);
                button_listened.Image = Properties.Resources.eye2;
            }
            else
            {
                Utility.current_user.Episode.Add(selectedNode.episode);
                button_listened.Image = Properties.Resources.eye;
            }

            Utility.dBase.SaveChanges();
        }
    }
}
