﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PodcastManagement.View
{
    public partial class frmAddEpisode : Form
    {
        private Type backFrm;
        private bool isNew = true;

        public frmAddEpisode(Type frmBack = null)
        {
            InitializeComponent();

            this.backFrm = frmBack;
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void button_cancel_Click(object sender, EventArgs e)
        {
            if(backFrm == null)
            {
                Utility.OpenForm(new MainForm(), this);
                return;
            }

            Utility.OpenForm((Form)Activator.CreateInstance(backFrm), this);
        }

        private void frmAddEpisode_Load(object sender, EventArgs e)
        {
            comboBox_podcast.DisplayMember = "PodcastRussianName";
            Utility.dBase.Podcast.ToList().ForEach(p => comboBox_podcast.Items.Add(p));

            comboBox_status.DisplayMember = "StatusName";
            Utility.dBase.Status.ToList().ForEach(s => comboBox_status.Items.Add(s));         
        }

        private void comboBox_podcast_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBox_podcast.SelectedItem != null)
            {
                comboBox_season.Items.Clear();
                comboBox_episode.Items.Clear();
                comboBox_season.DisplayMember = "SeasonName";
                (comboBox_podcast.SelectedItem as Podcast).Season.ToList().ForEach(s => comboBox_season.Items.Add(s));
            }
        }

        private void comboBox_season_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBox_season.SelectedItem != null)
            {
                comboBox_episode.Items.Clear();
                comboBox_episode.DisplayMember = "EpisodeNumber";
                (comboBox_season.SelectedItem as Season).Episode.ToList().ForEach(ep => comboBox_episode.Items.Add(ep));
            }
        }

        private void comboBox_episode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBox_episode.SelectedItem != null)
            {
                isNew = false;

                Episode ep = comboBox_episode.SelectedItem as Episode;

                textBox_number.Text = ep.EpisodeNumber;
                textBox_rusName.Text = ep.EpisodeRussianName;
                textBox_engName.Text = ep.EpisodeEnglishName;
                richTextBox_eng.Text = ep.Translation.TranslationEnglish;
                richTextBox_rus.Text = ep.Translation.TranslationRussian;
                textBox_description.Text = ep.EpisodeDescription;
                textBox_duration.Text = ep.EpisodeDuration.ToString();

                dateTimePicker.Value = ep.EpisodeDate;
                comboBox_status.SelectedItem = ep.Status;

                var uTrans = Utility.current_user.User_has_Translation.SingleOrDefault(t => t.Translation == ep.Translation);


                comboBox_role.SelectedItem = null;

                if(uTrans != null)
                    comboBox_role.SelectedItem = uTrans.UserActivity;
            }
        }

        private void button_newEpisode_Click(object sender, EventArgs e)
        {
            comboBox_episode.SelectedItem = null;
            textBox_number.Text = "";
            richTextBox_eng.Text = "";
            richTextBox_rus.Text = "";
            textBox_description.Text = "";
            textBox_duration.Text = "";
            textBox_engName.Text = "";
            textBox_rusName.Text = "";
            dateTimePicker.Value = DateTime.Now;
            comboBox_status.SelectedItem = null;
            comboBox_role.SelectedItem = null;

            isNew = true;
        }

        private void button_OK_Click(object sender, EventArgs e)
        {
            if(isNew)
            {
                using (var transaction = Utility.dBase.Database.BeginTransaction())
                {
                    try
                    {
                        Translation trans = new Translation();
                        trans.TranslationEndDate = DateTime.Now;
                        trans.TranslationEnglish = richTextBox_eng.Text;
                        trans.TranslationRussian = richTextBox_rus.Text;
                        trans.TranslationId = -1;

                        Utility.dBase.Translation.Add(trans);
                        Utility.dBase.SaveChanges();

                        Episode ep = new Episode();

                        ep.EpisodeDate = dateTimePicker.Value;
                        ep.EpisodeDescription = textBox_description.Text;
                        ep.EpisodeDuration = long.Parse(textBox_duration.Text);
                        ep.EpisodeEnglishName = textBox_engName.Text;
                        ep.EpisodeId = -1;
                        ep.EpisodeNumber = textBox_number.Text;
                        ep.EpisodeRussianName = textBox_rusName.Text;
                        ep.EpisodeStatus = comboBox_status.Text;
                        ep.Season = comboBox_season.SelectedItem as Season;
                        ep.Translation = trans;

                        Utility.dBase.Episode.Add(ep);
                        Utility.dBase.SaveChanges();

                        if (comboBox_role.SelectedItem != null && !string.IsNullOrEmpty(comboBox_role.Text))
                        {
                            Utility.current_user.User_has_Translation.Add(new User_has_Translation()
                            {
                                Translation = trans,
                                UserActivity = comboBox_role.Text
                                       
                            });

                            Utility.dBase.SaveChanges();
                        }

                        transaction.Commit();

                        MessageBox.Show(null, "Эпизод успешно добавлен", "Успех", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);

                        comboBox_episode.Items.Add(ep);
                    }
                    catch
                    {
                        transaction.Rollback();
                        MessageBox.Show(null, "Произошла ошибка при сохранении", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
            {
                if (comboBox_episode.SelectedItem != null)
                {
                    try
                    {
                        Episode ep = comboBox_episode.SelectedItem as Episode;

                        ep.EpisodeDate = dateTimePicker.Value;
                        ep.EpisodeDescription = textBox_description.Text;
                        ep.EpisodeDuration = long.Parse(textBox_duration.Text);
                        ep.EpisodeEnglishName = textBox_engName.Text;
                        ep.EpisodeNumber = textBox_number.Text;
                        ep.EpisodeRussianName = textBox_rusName.Text;
                        ep.EpisodeStatus = comboBox_status.Text;
                        ep.Translation.TranslationEndDate = DateTime.Now;
                        ep.Translation.TranslationEnglish = richTextBox_eng.Text;
                        ep.Translation.TranslationRussian = richTextBox_rus.Text;

                        if (comboBox_role.SelectedItem != null)
                        {
                            var res = Utility.current_user.User_has_Translation.SingleOrDefault(ut => ut.Translation_TranslationId == ep.Translation.TranslationId);

                            if (res != null)
                                if (string.IsNullOrEmpty(comboBox_role.Text))
                                    Utility.current_user.User_has_Translation.Remove(res);
                                else
                                    res.UserActivity = comboBox_role.Text;
                            else
                                Utility.current_user.User_has_Translation.Add(new User_has_Translation() { Translation = ep.Translation, UserActivity = comboBox_role.Text });
                        }

                        Utility.dBase.SaveChanges();

                        MessageBox.Show(null, "Эпизод успешно изменен", "Успех", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    
                    }
                    catch
                    {
                        MessageBox.Show("Ошибка");
                    }
                }
            }
        }
    }
}
