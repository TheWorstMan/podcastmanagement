﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PodcastManagement.View
{
    public partial class frmNavigation : Form
    {
        private List<MyPicContainer> podcast_pic;

        public frmNavigation()
        {
            InitializeComponent();
        }

        private void frmNavigation_Load(object sender, EventArgs e)
        {
            var podcasts = Utility.dBase.Podcast;

            podcast_pic = new List<MyPicContainer>();

            foreach (var p in podcasts)
            {
                Bitmap bmp;

                using (var mem = new MemoryStream(p.PodcastLogo))
                {
                    bmp = new Bitmap(mem);
                }

                MyPicContainer pb = new MyPicContainer(bmp, p);

                //PictureBox pb = new PictureBox();
                //pb.Image = bmp;
                pb.SizeMode = PictureBoxSizeMode.Zoom;
                pb.Width = 120;
                pb.Height = 120;
                pb.Click += Pb_Click;

                podcast_pic.Add(pb);
                //pb.Width = flowLayoutPanel_podcast.Width;
                //pb.Height = 0;

                this.flowLayoutPanel_podcast.Controls.Add(pb);
            }

            Pb_Click(podcast_pic.First(), null);

            if (Utility.current_user != null)
            {
                this.flowLayoutPanel_auth.Controls.Clear();

                foreach (var c in Utility.getUProfile(this))
                    this.flowLayoutPanel_auth.Controls.Add(c);
            }
        }

        private void Pb_Click(object sender, EventArgs e)
        {
            foreach (var p in podcast_pic)
                p.Selected = false;

            groupBox_episodes.Text = "";


            MyPicContainer pic = sender as MyPicContainer;

            pic.Selected = true;
            groupBox_episodes.Text = pic.podcast.PodcastRussianName;

            if(Utility.current_user != null)
                if(Utility.current_user.Podcast.Any(p => p == pic.podcast))
                {
                    button_favorite.Text = "Слушаю";
                    button_favorite.Image = Properties.Resources.favorite_selected;
                }
                else
                {
                    button_favorite.Text = "Слушать";
                    button_favorite.Image = Properties.Resources.favorite;
                }

            /* Episode episode = null;

             try
             {

                 foreach (var s in pic.podcast.Season.OrderByDescending(o => o.SeasonStartDate))
                 {
                     if (s.Episode.Count <= 0)
                         continue;

                     var epi = s.Episode
                         .Where(ep => ep.TranslationId != null && ep.Translation.TranslationRussian != null)
                         .OrderByDescending(ep => ep.EpisodeDate);

                     if (epi.Count() <= 0)
                         continue;

                     episode = epi.First();

                     break;
                 }


             }
             catch { }
             */
            label_creat.Text = "";
            label_transl.Text = "";

            try
            {
                foreach (var cr in pic.podcast.Creator)
                    label_creat.Text += cr.CreatorName + "\n";

                foreach (var tr in Utility.dBase.User
                    .Where(u => u.User_has_Translation.Any(ut => ut.Translation.Episode.Any(ep => ep.Season.PodcastId == pic.podcast.PodcastId))))

                    label_transl.Text += tr.UserLogin + "\n";

                label_eng.Text = pic.podcast.PodcastEnglishName;
                label_description.Text = pic.podcast.PodcastDescription;
                label_date.Text = string.Format("{0} {1} Статус: {2}", 
                    pic.podcast.PodcastEndDate == null ? pic.podcast.PodcastStartDate.ToString("Дата начала: dd.MM.yyyy") : pic.podcast.PodcastStartDate.ToString("dd.MM.yyyy"), 
                    pic.podcast.PodcastEndDate != null ? pic.podcast.PodcastEndDate.Value.ToString("- dd.MM.yyyy") : "",
                    pic.podcast.PodcastStatus
                    );

                linkLabel_engsite.Text = pic.podcast.PodcastTranscript;
                linkLabel_site.Text = pic.podcast.PodcastSite;

                //foreach (var s in pic.podcast.Season.OrderBy(o => o.SeasonStartDate))
                //{
                //    if (s.Episode.Count <= 0)
                //        continue;

                //    var epi = s.Episode
                //        .Where(ep => ep.TranslationId != null && ep.Translation.TranslationRussian != null)
                //        .OrderByDescending(ep => ep.EpisodeDate);

                //    if (epi.Count() <= 0)
                //        continue;

                //    foreach (var ep in epi)
                //    {
                //        var tr = ep.Translation.;

                //    }

                //    break;
                //}
               // label_eng.Text = "";

               
            }
            catch { }
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            if (!Utility.Auth(textBoxLogin.Text, textBoxPassword.Text))
            {
                MessageBox.Show(null, "Логин или пароль несовпадают", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            this.groupBox_login.Text = "Личный Кабинет";
            this.flowLayoutPanel_auth.Controls.Clear();

            foreach (var c in Utility.getUProfile(this))
                this.flowLayoutPanel_auth.Controls.Add(c);
        }

        private void buttonRegistration_Click(object sender, EventArgs e)
        {
            Utility.OpenForm(new Auth.frmRegistration(this), this);
        }

        private void button_navigation_Click(object sender, EventArgs e)
        {
            Utility.OpenForm(new MainForm(), this);
        }

        private void button_favorite_Click(object sender, EventArgs e)
        {
            if(Utility.current_user == null)
            {
                MessageBox.Show(null, "Для этого действия вам необходимо авторизоваться", "Ошибка доступа", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            var podcast = podcast_pic.Where(pp => pp.Selected == true).First().podcast;

            try
            {
                if (Utility.current_user.Podcast.Any(p => p == podcast))
                    Utility.current_user.Podcast.Remove(podcast);
                else
                    Utility.current_user.Podcast.Add(podcast);

                Utility.dBase.SaveChanges();

                if (Utility.current_user.Podcast.Any(p => p == podcast))
                {
                    button_favorite.Text = "Слушаю";
                    button_favorite.Image = Properties.Resources.favorite_selected;
                }
                else
                {
                    button_favorite.Text = "Слушать";
                    button_favorite.Image = Properties.Resources.favorite;
                }
            }
            catch
            {
                MessageBox.Show(null, "Произошла ошибка при сохранении", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button_episode_Click(object sender, EventArgs e)
        {
            Utility.OpenForm(new View.frmEpisodes(podcast_pic.Where(p => p.Selected == true).First().podcast, backForm:typeof(frmNavigation)), this);
        }

        private void button_characters_Click(object sender, EventArgs e)
        {
            Utility.OpenForm(new View.frmEpisodeCharacters(podcast_pic.Where(p => p.Selected == true).First().podcast, typeof(frmNavigation)), this);
        }
    }
}
