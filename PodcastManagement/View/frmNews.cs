﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PodcastManagement.View
{
    public partial class frmNews : Form
    {
        public frmNews()
        {
            InitializeComponent();
        }

        private void frmNews_Load(object sender, EventArgs e)
        {
            if (Utility.current_user != null)
            {
                this.flowLayoutPanel_auth.Controls.Clear();

                foreach (var c in Utility.getUProfile(this))
                    this.flowLayoutPanel_auth.Controls.Add(c);
            }

            foreach (var n in Utility.getAllNews())
                this.flowLayoutPanel_news.Controls.Add(n);
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            if (!Utility.Auth(textBoxLogin.Text, textBoxPassword.Text))
            {
                MessageBox.Show(null, "Логин или пароль несовпадают", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            this.groupBox_login.Text = "Личный Кабинет";
            this.flowLayoutPanel_auth.Controls.Clear();

            foreach (var c in Utility.getUProfile(this))
                this.flowLayoutPanel_auth.Controls.Add(c);
        }

        private void buttonRegistration_Click(object sender, EventArgs e)
        {
            Utility.OpenForm(new Auth.frmRegistration(this), this);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Utility.OpenForm(new MainForm(), this);
        }

       
    }
}
