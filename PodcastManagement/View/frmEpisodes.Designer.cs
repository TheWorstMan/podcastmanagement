﻿namespace PodcastManagement.View
{
    partial class frmEpisodes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.treeView = new System.Windows.Forms.TreeView();
            this.groupBox_login = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel_auth = new System.Windows.Forms.FlowLayoutPanel();
            this.labelLogin = new System.Windows.Forms.Label();
            this.textBoxLogin = new System.Windows.Forms.TextBox();
            this.labelPassword = new System.Windows.Forms.Label();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.buttonLogin = new System.Windows.Forms.Button();
            this.buttonRegistration = new System.Windows.Forms.Button();
            this.groupBox_episode = new System.Windows.Forms.GroupBox();
            this.button_listened = new System.Windows.Forms.Button();
            this.button_episode_trans = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.richTextBox_episodeText = new System.Windows.Forms.RichTextBox();
            this.buttonBack = new System.Windows.Forms.Button();
            this.groupBox2.SuspendLayout();
            this.groupBox_login.SuspendLayout();
            this.flowLayoutPanel_auth.SuspendLayout();
            this.groupBox_episode.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.treeView);
            this.groupBox2.Font = new System.Drawing.Font("BigNoodleTitlingCyr", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox2.Location = new System.Drawing.Point(475, 9);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(258, 275);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Эпизоды";
            // 
            // treeView
            // 
            this.treeView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(237)))), ((int)(((byte)(243)))));
            this.treeView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.treeView.Font = new System.Drawing.Font("Panton", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.treeView.HotTracking = true;
            this.treeView.Location = new System.Drawing.Point(1, 25);
            this.treeView.Margin = new System.Windows.Forms.Padding(2);
            this.treeView.Name = "treeView";
            this.treeView.Size = new System.Drawing.Size(249, 237);
            this.treeView.TabIndex = 11;
            this.treeView.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView_NodeMouseClick);
            // 
            // groupBox_login
            // 
            this.groupBox_login.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox_login.BackColor = System.Drawing.Color.Transparent;
            this.groupBox_login.Controls.Add(this.flowLayoutPanel_auth);
            this.groupBox_login.Font = new System.Drawing.Font("BigNoodleTitlingCyr", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox_login.Location = new System.Drawing.Point(475, 291);
            this.groupBox_login.Name = "groupBox_login";
            this.groupBox_login.Size = new System.Drawing.Size(258, 185);
            this.groupBox_login.TabIndex = 11;
            this.groupBox_login.TabStop = false;
            this.groupBox_login.Text = "Авторизация";
            // 
            // flowLayoutPanel_auth
            // 
            this.flowLayoutPanel_auth.Controls.Add(this.labelLogin);
            this.flowLayoutPanel_auth.Controls.Add(this.textBoxLogin);
            this.flowLayoutPanel_auth.Controls.Add(this.labelPassword);
            this.flowLayoutPanel_auth.Controls.Add(this.textBoxPassword);
            this.flowLayoutPanel_auth.Controls.Add(this.buttonLogin);
            this.flowLayoutPanel_auth.Controls.Add(this.buttonRegistration);
            this.flowLayoutPanel_auth.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel_auth.Location = new System.Drawing.Point(3, 33);
            this.flowLayoutPanel_auth.Name = "flowLayoutPanel_auth";
            this.flowLayoutPanel_auth.Size = new System.Drawing.Size(252, 149);
            this.flowLayoutPanel_auth.TabIndex = 2;
            // 
            // labelLogin
            // 
            this.labelLogin.Font = new System.Drawing.Font("Panton", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLogin.Location = new System.Drawing.Point(3, 0);
            this.labelLogin.Name = "labelLogin";
            this.labelLogin.Size = new System.Drawing.Size(96, 30);
            this.labelLogin.TabIndex = 0;
            this.labelLogin.Text = "Логин";
            this.labelLogin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxLogin
            // 
            this.textBoxLogin.Font = new System.Drawing.Font("Panton SemiBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxLogin.Location = new System.Drawing.Point(105, 3);
            this.textBoxLogin.Name = "textBoxLogin";
            this.textBoxLogin.Size = new System.Drawing.Size(144, 27);
            this.textBoxLogin.TabIndex = 1;
            // 
            // labelPassword
            // 
            this.labelPassword.Font = new System.Drawing.Font("Panton", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelPassword.Location = new System.Drawing.Point(3, 33);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(96, 30);
            this.labelPassword.TabIndex = 2;
            this.labelPassword.Text = "Пароль";
            this.labelPassword.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Font = new System.Drawing.Font("Panton SemiBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxPassword.Location = new System.Drawing.Point(105, 36);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.PasswordChar = '*';
            this.textBoxPassword.Size = new System.Drawing.Size(144, 27);
            this.textBoxPassword.TabIndex = 3;
            // 
            // buttonLogin
            // 
            this.buttonLogin.BackColor = System.Drawing.Color.Transparent;
            this.buttonLogin.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(175)))), ((int)(((byte)(211)))));
            this.buttonLogin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonLogin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(217)))), ((int)(((byte)(234)))));
            this.buttonLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLogin.Font = new System.Drawing.Font("Panton", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonLogin.Location = new System.Drawing.Point(3, 69);
            this.buttonLogin.Name = "buttonLogin";
            this.buttonLogin.Size = new System.Drawing.Size(119, 31);
            this.buttonLogin.TabIndex = 5;
            this.buttonLogin.Text = "Войти";
            this.buttonLogin.UseVisualStyleBackColor = false;
            // 
            // buttonRegistration
            // 
            this.buttonRegistration.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(175)))), ((int)(((byte)(211)))));
            this.buttonRegistration.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonRegistration.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(217)))), ((int)(((byte)(234)))));
            this.buttonRegistration.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRegistration.Font = new System.Drawing.Font("Panton", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonRegistration.Location = new System.Drawing.Point(128, 69);
            this.buttonRegistration.Name = "buttonRegistration";
            this.buttonRegistration.Size = new System.Drawing.Size(121, 31);
            this.buttonRegistration.TabIndex = 4;
            this.buttonRegistration.Text = "Регистрация";
            this.buttonRegistration.UseVisualStyleBackColor = true;
            // 
            // groupBox_episode
            // 
            this.groupBox_episode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox_episode.BackColor = System.Drawing.Color.Transparent;
            this.groupBox_episode.Controls.Add(this.button_listened);
            this.groupBox_episode.Controls.Add(this.button_episode_trans);
            this.groupBox_episode.Controls.Add(this.panel1);
            this.groupBox_episode.Font = new System.Drawing.Font("BigNoodleTitlingCyr", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox_episode.Location = new System.Drawing.Point(9, 9);
            this.groupBox_episode.Name = "groupBox_episode";
            this.groupBox_episode.Size = new System.Drawing.Size(461, 398);
            this.groupBox_episode.TabIndex = 12;
            this.groupBox_episode.TabStop = false;
            this.groupBox_episode.Text = "ЭПИЗОД";
            // 
            // button_listened
            // 
            this.button_listened.BackColor = System.Drawing.Color.Transparent;
            this.button_listened.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(175)))), ((int)(((byte)(211)))));
            this.button_listened.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button_listened.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(217)))), ((int)(((byte)(234)))));
            this.button_listened.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_listened.Font = new System.Drawing.Font("Panton", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_listened.Image = global::PodcastManagement.Properties.Resources.eye2;
            this.button_listened.Location = new System.Drawing.Point(369, 0);
            this.button_listened.Name = "button_listened";
            this.button_listened.Size = new System.Drawing.Size(31, 31);
            this.button_listened.TabIndex = 16;
            this.button_listened.UseVisualStyleBackColor = false;
            this.button_listened.Click += new System.EventHandler(this.button_listened_Click);
            // 
            // button_episode_trans
            // 
            this.button_episode_trans.BackColor = System.Drawing.Color.Transparent;
            this.button_episode_trans.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(175)))), ((int)(((byte)(211)))));
            this.button_episode_trans.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button_episode_trans.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(217)))), ((int)(((byte)(234)))));
            this.button_episode_trans.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_episode_trans.Font = new System.Drawing.Font("Panton", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_episode_trans.Location = new System.Drawing.Point(399, 0);
            this.button_episode_trans.Name = "button_episode_trans";
            this.button_episode_trans.Size = new System.Drawing.Size(61, 31);
            this.button_episode_trans.TabIndex = 13;
            this.button_episode_trans.Text = "ENG";
            this.button_episode_trans.UseVisualStyleBackColor = false;
            this.button_episode_trans.Click += new System.EventHandler(this.button_episode_trans_Click);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.Controls.Add(this.richTextBox_episodeText);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 33);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(455, 362);
            this.panel1.TabIndex = 15;
            // 
            // richTextBox_episodeText
            // 
            this.richTextBox_episodeText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox_episodeText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox_episodeText.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.richTextBox_episodeText.Location = new System.Drawing.Point(0, 0);
            this.richTextBox_episodeText.Margin = new System.Windows.Forms.Padding(2);
            this.richTextBox_episodeText.Name = "richTextBox_episodeText";
            this.richTextBox_episodeText.ReadOnly = true;
            this.richTextBox_episodeText.Size = new System.Drawing.Size(455, 362);
            this.richTextBox_episodeText.TabIndex = 15;
            this.richTextBox_episodeText.Text = "";
            this.richTextBox_episodeText.TextChanged += new System.EventHandler(this.richTextBox_episodeText_TextChanged);
            // 
            // buttonBack
            // 
            this.buttonBack.BackColor = System.Drawing.Color.Transparent;
            this.buttonBack.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(175)))), ((int)(((byte)(211)))));
            this.buttonBack.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonBack.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(217)))), ((int)(((byte)(234)))));
            this.buttonBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBack.Font = new System.Drawing.Font("Panton", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonBack.Location = new System.Drawing.Point(349, 409);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(119, 31);
            this.buttonBack.TabIndex = 6;
            this.buttonBack.Text = "Назад";
            this.buttonBack.UseVisualStyleBackColor = false;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // frmEpisodes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::PodcastManagement.Properties.Resources.background;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(742, 480);
            this.Controls.Add(this.groupBox_episode);
            this.Controls.Add(this.groupBox_login);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.buttonBack);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmEpisodes";
            this.Text = "frmEpisodes";
            this.Load += new System.EventHandler(this.frmEpisodes_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox_login.ResumeLayout(false);
            this.flowLayoutPanel_auth.ResumeLayout(false);
            this.flowLayoutPanel_auth.PerformLayout();
            this.groupBox_episode.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TreeView treeView;
        private System.Windows.Forms.GroupBox groupBox_login;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel_auth;
        private System.Windows.Forms.Label labelLogin;
        private System.Windows.Forms.TextBox textBoxLogin;
        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.Button buttonLogin;
        private System.Windows.Forms.Button buttonRegistration;
        private System.Windows.Forms.GroupBox groupBox_episode;
        private System.Windows.Forms.Button button_episode_trans;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RichTextBox richTextBox_episodeText;
        private System.Windows.Forms.Button buttonBack;
        private System.Windows.Forms.Button button_listened;
    }
}