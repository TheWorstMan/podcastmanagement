﻿namespace PodcastManagement.View
{
    partial class frmNavigation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_favorite = new System.Windows.Forms.Button();
            this.groupBox_login = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel_auth = new System.Windows.Forms.FlowLayoutPanel();
            this.labelLogin = new System.Windows.Forms.Label();
            this.textBoxLogin = new System.Windows.Forms.TextBox();
            this.labelPassword = new System.Windows.Forms.Label();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.buttonLogin = new System.Windows.Forms.Button();
            this.buttonRegistration = new System.Windows.Forms.Button();
            this.groupBox_podcast = new System.Windows.Forms.GroupBox();
            this.button_navigation = new System.Windows.Forms.Button();
            this.flowLayoutPanel_podcast = new System.Windows.Forms.FlowLayoutPanel();
            this.groupBox_episodes = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel_EpList = new System.Windows.Forms.FlowLayoutPanel();
            this.label_eng = new System.Windows.Forms.Label();
            this.label_date = new System.Windows.Forms.Label();
            this.label_description = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.linkLabel_engsite = new System.Windows.Forms.LinkLabel();
            this.label3 = new System.Windows.Forms.Label();
            this.linkLabel_site = new System.Windows.Forms.LinkLabel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel_authors = new System.Windows.Forms.FlowLayoutPanel();
            this.label_creat = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel_translators = new System.Windows.Forms.FlowLayoutPanel();
            this.label_transl = new System.Windows.Forms.Label();
            this.button_episode = new System.Windows.Forms.Button();
            this.button_characters = new System.Windows.Forms.Button();
            this.groupBox_login.SuspendLayout();
            this.flowLayoutPanel_auth.SuspendLayout();
            this.groupBox_podcast.SuspendLayout();
            this.groupBox_episodes.SuspendLayout();
            this.flowLayoutPanel_EpList.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.flowLayoutPanel_authors.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.flowLayoutPanel_translators.SuspendLayout();
            this.SuspendLayout();
            // 
            // button_favorite
            // 
            this.button_favorite.BackColor = System.Drawing.Color.White;
            this.button_favorite.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button_favorite.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(175)))), ((int)(((byte)(211)))));
            this.button_favorite.FlatAppearance.MouseDownBackColor = System.Drawing.Color.GhostWhite;
            this.button_favorite.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(217)))), ((int)(((byte)(234)))));
            this.button_favorite.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_favorite.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_favorite.Image = global::PodcastManagement.Properties.Resources.favorite;
            this.button_favorite.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button_favorite.Location = new System.Drawing.Point(640, 0);
            this.button_favorite.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button_favorite.Name = "button_favorite";
            this.button_favorite.Size = new System.Drawing.Size(153, 46);
            this.button_favorite.TabIndex = 5;
            this.button_favorite.Text = "Слушать";
            this.button_favorite.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button_favorite.UseVisualStyleBackColor = false;
            this.button_favorite.Click += new System.EventHandler(this.button_favorite_Click);
            // 
            // groupBox_login
            // 
            this.groupBox_login.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox_login.BackColor = System.Drawing.Color.Transparent;
            this.groupBox_login.Controls.Add(this.flowLayoutPanel_auth);
            this.groupBox_login.Font = new System.Drawing.Font("BigNoodleTitlingCyr", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox_login.Location = new System.Drawing.Point(825, 460);
            this.groupBox_login.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox_login.Name = "groupBox_login";
            this.groupBox_login.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox_login.Size = new System.Drawing.Size(387, 285);
            this.groupBox_login.TabIndex = 7;
            this.groupBox_login.TabStop = false;
            this.groupBox_login.Text = "Авторизация";
            // 
            // flowLayoutPanel_auth
            // 
            this.flowLayoutPanel_auth.Controls.Add(this.labelLogin);
            this.flowLayoutPanel_auth.Controls.Add(this.textBoxLogin);
            this.flowLayoutPanel_auth.Controls.Add(this.labelPassword);
            this.flowLayoutPanel_auth.Controls.Add(this.textBoxPassword);
            this.flowLayoutPanel_auth.Controls.Add(this.buttonLogin);
            this.flowLayoutPanel_auth.Controls.Add(this.buttonRegistration);
            this.flowLayoutPanel_auth.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel_auth.Location = new System.Drawing.Point(4, 35);
            this.flowLayoutPanel_auth.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.flowLayoutPanel_auth.Name = "flowLayoutPanel_auth";
            this.flowLayoutPanel_auth.Size = new System.Drawing.Size(379, 245);
            this.flowLayoutPanel_auth.TabIndex = 2;
            // 
            // labelLogin
            // 
            this.labelLogin.Font = new System.Drawing.Font("Panton", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLogin.Location = new System.Drawing.Point(4, 0);
            this.labelLogin.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelLogin.Name = "labelLogin";
            this.labelLogin.Size = new System.Drawing.Size(144, 46);
            this.labelLogin.TabIndex = 0;
            this.labelLogin.Text = "Логин";
            this.labelLogin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxLogin
            // 
            this.textBoxLogin.Font = new System.Drawing.Font("Panton SemiBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxLogin.Location = new System.Drawing.Point(156, 5);
            this.textBoxLogin.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBoxLogin.Name = "textBoxLogin";
            this.textBoxLogin.Size = new System.Drawing.Size(214, 27);
            this.textBoxLogin.TabIndex = 1;
            // 
            // labelPassword
            // 
            this.labelPassword.Font = new System.Drawing.Font("Panton", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelPassword.Location = new System.Drawing.Point(4, 46);
            this.labelPassword.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(144, 46);
            this.labelPassword.TabIndex = 2;
            this.labelPassword.Text = "Пароль";
            this.labelPassword.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Font = new System.Drawing.Font("Panton SemiBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxPassword.Location = new System.Drawing.Point(156, 51);
            this.textBoxPassword.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.PasswordChar = '*';
            this.textBoxPassword.Size = new System.Drawing.Size(214, 27);
            this.textBoxPassword.TabIndex = 3;
            // 
            // buttonLogin
            // 
            this.buttonLogin.BackColor = System.Drawing.Color.Transparent;
            this.buttonLogin.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(175)))), ((int)(((byte)(211)))));
            this.buttonLogin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonLogin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(217)))), ((int)(((byte)(234)))));
            this.buttonLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLogin.Font = new System.Drawing.Font("Panton", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonLogin.Location = new System.Drawing.Point(4, 97);
            this.buttonLogin.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonLogin.Name = "buttonLogin";
            this.buttonLogin.Size = new System.Drawing.Size(178, 48);
            this.buttonLogin.TabIndex = 5;
            this.buttonLogin.Text = "Войти";
            this.buttonLogin.UseVisualStyleBackColor = false;
            this.buttonLogin.Click += new System.EventHandler(this.buttonLogin_Click);
            // 
            // buttonRegistration
            // 
            this.buttonRegistration.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(175)))), ((int)(((byte)(211)))));
            this.buttonRegistration.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonRegistration.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(217)))), ((int)(((byte)(234)))));
            this.buttonRegistration.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRegistration.Font = new System.Drawing.Font("Panton", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonRegistration.Location = new System.Drawing.Point(190, 97);
            this.buttonRegistration.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonRegistration.Name = "buttonRegistration";
            this.buttonRegistration.Size = new System.Drawing.Size(182, 48);
            this.buttonRegistration.TabIndex = 4;
            this.buttonRegistration.Text = "Регистрация";
            this.buttonRegistration.UseVisualStyleBackColor = true;
            this.buttonRegistration.Click += new System.EventHandler(this.buttonRegistration_Click);
            // 
            // groupBox_podcast
            // 
            this.groupBox_podcast.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox_podcast.BackColor = System.Drawing.Color.Transparent;
            this.groupBox_podcast.Controls.Add(this.button_navigation);
            this.groupBox_podcast.Controls.Add(this.flowLayoutPanel_podcast);
            this.groupBox_podcast.Font = new System.Drawing.Font("BigNoodleTitlingCyr", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox_podcast.Location = new System.Drawing.Point(825, 3);
            this.groupBox_podcast.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox_podcast.Name = "groupBox_podcast";
            this.groupBox_podcast.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox_podcast.Size = new System.Drawing.Size(387, 452);
            this.groupBox_podcast.TabIndex = 5;
            this.groupBox_podcast.TabStop = false;
            this.groupBox_podcast.Text = "Подкасты";
            // 
            // button_navigation
            // 
            this.button_navigation.AutoSize = true;
            this.button_navigation.BackColor = System.Drawing.Color.White;
            this.button_navigation.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(175)))), ((int)(((byte)(211)))));
            this.button_navigation.FlatAppearance.MouseDownBackColor = System.Drawing.Color.GhostWhite;
            this.button_navigation.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(217)))), ((int)(((byte)(234)))));
            this.button_navigation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_navigation.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_navigation.Location = new System.Drawing.Point(171, 0);
            this.button_navigation.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button_navigation.Name = "button_navigation";
            this.button_navigation.Size = new System.Drawing.Size(216, 46);
            this.button_navigation.TabIndex = 4;
            this.button_navigation.Text = "На главную";
            this.button_navigation.UseVisualStyleBackColor = false;
            this.button_navigation.Click += new System.EventHandler(this.button_navigation_Click);
            // 
            // flowLayoutPanel_podcast
            // 
            this.flowLayoutPanel_podcast.AutoScroll = true;
            this.flowLayoutPanel_podcast.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel_podcast.Location = new System.Drawing.Point(4, 35);
            this.flowLayoutPanel_podcast.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.flowLayoutPanel_podcast.Name = "flowLayoutPanel_podcast";
            this.flowLayoutPanel_podcast.Size = new System.Drawing.Size(379, 412);
            this.flowLayoutPanel_podcast.TabIndex = 2;
            // 
            // groupBox_episodes
            // 
            this.groupBox_episodes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox_episodes.BackColor = System.Drawing.Color.Transparent;
            this.groupBox_episodes.Controls.Add(this.button_favorite);
            this.groupBox_episodes.Controls.Add(this.flowLayoutPanel_EpList);
            this.groupBox_episodes.Font = new System.Drawing.Font("BigNoodleTitlingCyr", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox_episodes.Location = new System.Drawing.Point(18, 3);
            this.groupBox_episodes.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox_episodes.Name = "groupBox_episodes";
            this.groupBox_episodes.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox_episodes.Size = new System.Drawing.Size(794, 335);
            this.groupBox_episodes.TabIndex = 8;
            this.groupBox_episodes.TabStop = false;
            this.groupBox_episodes.Text = "Список эпизодов";
            // 
            // flowLayoutPanel_EpList
            // 
            this.flowLayoutPanel_EpList.AutoScroll = true;
            this.flowLayoutPanel_EpList.Controls.Add(this.label_eng);
            this.flowLayoutPanel_EpList.Controls.Add(this.label_date);
            this.flowLayoutPanel_EpList.Controls.Add(this.label_description);
            this.flowLayoutPanel_EpList.Controls.Add(this.panel1);
            this.flowLayoutPanel_EpList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel_EpList.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel_EpList.Location = new System.Drawing.Point(4, 35);
            this.flowLayoutPanel_EpList.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.flowLayoutPanel_EpList.Name = "flowLayoutPanel_EpList";
            this.flowLayoutPanel_EpList.Size = new System.Drawing.Size(786, 295);
            this.flowLayoutPanel_EpList.TabIndex = 2;
            // 
            // label_eng
            // 
            this.label_eng.AutoSize = true;
            this.label_eng.Font = new System.Drawing.Font("Panton", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_eng.Location = new System.Drawing.Point(4, 0);
            this.label_eng.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_eng.Name = "label_eng";
            this.label_eng.Size = new System.Drawing.Size(119, 25);
            this.label_eng.TabIndex = 1;
            this.label_eng.Text = "English Title";
            // 
            // label_date
            // 
            this.label_date.AutoSize = true;
            this.label_date.Font = new System.Drawing.Font("Panton", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_date.Location = new System.Drawing.Point(3, 31);
            this.label_date.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label_date.Name = "label_date";
            this.label_date.Size = new System.Drawing.Size(140, 16);
            this.label_date.TabIndex = 2;
            this.label_date.Text = "дата1 - дата2 - статус";
            this.label_date.Click += new System.EventHandler(this.label1_Click);
            // 
            // label_description
            // 
            this.label_description.AutoSize = true;
            this.label_description.Font = new System.Drawing.Font("Panton", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_description.Location = new System.Drawing.Point(3, 53);
            this.label_description.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label_description.Name = "label_description";
            this.label_description.Size = new System.Drawing.Size(80, 19);
            this.label_description.TabIndex = 3;
            this.label_description.Text = "Описание";
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.linkLabel_engsite);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.linkLabel_site);
            this.panel1.Location = new System.Drawing.Point(3, 82);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(306, 61);
            this.panel1.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Panton", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(3, 42);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(151, 19);
            this.label4.TabIndex = 6;
            this.label4.Text = "Английские тексты:";
            // 
            // linkLabel_engsite
            // 
            this.linkLabel_engsite.AutoSize = true;
            this.linkLabel_engsite.Font = new System.Drawing.Font("Panton", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel_engsite.Location = new System.Drawing.Point(226, 42);
            this.linkLabel_engsite.Name = "linkLabel_engsite";
            this.linkLabel_engsite.Size = new System.Drawing.Size(77, 19);
            this.linkLabel_engsite.TabIndex = 7;
            this.linkLabel_engsite.TabStop = true;
            this.linkLabel_engsite.Text = "linkLabel2";
            this.linkLabel_engsite.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Panton", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(3, 6);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(117, 19);
            this.label3.TabIndex = 4;
            this.label3.Text = "Сайт подкаста:";
            // 
            // linkLabel_site
            // 
            this.linkLabel_site.AutoSize = true;
            this.linkLabel_site.Font = new System.Drawing.Font("Panton", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel_site.Location = new System.Drawing.Point(181, 6);
            this.linkLabel_site.Name = "linkLabel_site";
            this.linkLabel_site.Size = new System.Drawing.Size(75, 19);
            this.linkLabel_site.TabIndex = 5;
            this.linkLabel_site.TabStop = true;
            this.linkLabel_site.Text = "linkLabel1";
            this.linkLabel_site.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.flowLayoutPanel_authors);
            this.groupBox2.Font = new System.Drawing.Font("BigNoodleTitlingCyr", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox2.Location = new System.Drawing.Point(14, 343);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Size = new System.Drawing.Size(396, 301);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Авторы";
            // 
            // flowLayoutPanel_authors
            // 
            this.flowLayoutPanel_authors.AutoScroll = true;
            this.flowLayoutPanel_authors.Controls.Add(this.label_creat);
            this.flowLayoutPanel_authors.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel_authors.Location = new System.Drawing.Point(4, 35);
            this.flowLayoutPanel_authors.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.flowLayoutPanel_authors.Name = "flowLayoutPanel_authors";
            this.flowLayoutPanel_authors.Size = new System.Drawing.Size(388, 261);
            this.flowLayoutPanel_authors.TabIndex = 2;
            // 
            // label_creat
            // 
            this.label_creat.AutoSize = true;
            this.label_creat.Font = new System.Drawing.Font("Panton", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_creat.Location = new System.Drawing.Point(4, 0);
            this.label_creat.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_creat.Name = "label_creat";
            this.label_creat.Size = new System.Drawing.Size(47, 19);
            this.label_creat.TabIndex = 1;
            this.label_creat.Text = "label1";
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.flowLayoutPanel_translators);
            this.groupBox3.Font = new System.Drawing.Font("BigNoodleTitlingCyr", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox3.Location = new System.Drawing.Point(416, 343);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox3.Size = new System.Drawing.Size(396, 301);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Переводчики";
            // 
            // flowLayoutPanel_translators
            // 
            this.flowLayoutPanel_translators.AutoScroll = true;
            this.flowLayoutPanel_translators.Controls.Add(this.label_transl);
            this.flowLayoutPanel_translators.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel_translators.Location = new System.Drawing.Point(4, 35);
            this.flowLayoutPanel_translators.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.flowLayoutPanel_translators.Name = "flowLayoutPanel_translators";
            this.flowLayoutPanel_translators.Size = new System.Drawing.Size(388, 261);
            this.flowLayoutPanel_translators.TabIndex = 2;
            // 
            // label_transl
            // 
            this.label_transl.AutoSize = true;
            this.label_transl.Font = new System.Drawing.Font("Panton", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_transl.Location = new System.Drawing.Point(4, 0);
            this.label_transl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_transl.Name = "label_transl";
            this.label_transl.Size = new System.Drawing.Size(47, 19);
            this.label_transl.TabIndex = 0;
            this.label_transl.Text = "label1";
            // 
            // button_episode
            // 
            this.button_episode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button_episode.BackColor = System.Drawing.Color.White;
            this.button_episode.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button_episode.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(175)))), ((int)(((byte)(211)))));
            this.button_episode.FlatAppearance.MouseDownBackColor = System.Drawing.Color.GhostWhite;
            this.button_episode.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(217)))), ((int)(((byte)(234)))));
            this.button_episode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_episode.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_episode.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button_episode.Location = new System.Drawing.Point(658, 288);
            this.button_episode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button_episode.Name = "button_episode";
            this.button_episode.Size = new System.Drawing.Size(153, 46);
            this.button_episode.TabIndex = 6;
            this.button_episode.Text = "Эпизоды";
            this.button_episode.UseVisualStyleBackColor = false;
            this.button_episode.Click += new System.EventHandler(this.button_episode_Click);
            // 
            // button_characters
            // 
            this.button_characters.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button_characters.BackColor = System.Drawing.Color.White;
            this.button_characters.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button_characters.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(175)))), ((int)(((byte)(211)))));
            this.button_characters.FlatAppearance.MouseDownBackColor = System.Drawing.Color.GhostWhite;
            this.button_characters.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(217)))), ((int)(((byte)(234)))));
            this.button_characters.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_characters.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_characters.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button_characters.Location = new System.Drawing.Point(506, 288);
            this.button_characters.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button_characters.Name = "button_characters";
            this.button_characters.Size = new System.Drawing.Size(153, 46);
            this.button_characters.TabIndex = 11;
            this.button_characters.Text = "Персонажи";
            this.button_characters.UseVisualStyleBackColor = false;
            this.button_characters.Click += new System.EventHandler(this.button_characters_Click);
            // 
            // frmNavigation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::PodcastManagement.Properties.Resources.background;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1215, 759);
            this.Controls.Add(this.button_characters);
            this.Controls.Add(this.button_episode);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox_episodes);
            this.Controls.Add(this.groupBox_login);
            this.Controls.Add(this.groupBox_podcast);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmNavigation";
            this.Text = "Навигация";
            this.Load += new System.EventHandler(this.frmNavigation_Load);
            this.groupBox_login.ResumeLayout(false);
            this.flowLayoutPanel_auth.ResumeLayout(false);
            this.flowLayoutPanel_auth.PerformLayout();
            this.groupBox_podcast.ResumeLayout(false);
            this.groupBox_podcast.PerformLayout();
            this.groupBox_episodes.ResumeLayout(false);
            this.flowLayoutPanel_EpList.ResumeLayout(false);
            this.flowLayoutPanel_EpList.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.flowLayoutPanel_authors.ResumeLayout(false);
            this.flowLayoutPanel_authors.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.flowLayoutPanel_translators.ResumeLayout(false);
            this.flowLayoutPanel_translators.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox_login;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel_auth;
        private System.Windows.Forms.Label labelLogin;
        private System.Windows.Forms.TextBox textBoxLogin;
        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.Button buttonLogin;
        private System.Windows.Forms.Button buttonRegistration;
        private System.Windows.Forms.GroupBox groupBox_podcast;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel_podcast;
        private System.Windows.Forms.GroupBox groupBox_episodes;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel_EpList;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel_authors;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel_translators;
        private System.Windows.Forms.Label label_transl;
        private System.Windows.Forms.Label label_creat;
        private System.Windows.Forms.Label label_eng;
        private System.Windows.Forms.Button button_navigation;
        private System.Windows.Forms.Button button_favorite;
        private System.Windows.Forms.Label label_date;
        private System.Windows.Forms.Label label_description;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.LinkLabel linkLabel_engsite;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.LinkLabel linkLabel_site;
        private System.Windows.Forms.Button button_episode;
        private System.Windows.Forms.Button button_characters;
    }
}