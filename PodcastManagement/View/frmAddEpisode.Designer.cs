﻿namespace PodcastManagement.View
{
    partial class frmAddEpisode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBox_eng = new System.Windows.Forms.RichTextBox();
            this.richTextBox_rus = new System.Windows.Forms.RichTextBox();
            this.textBox_number = new System.Windows.Forms.TextBox();
            this.textBox_engName = new System.Windows.Forms.TextBox();
            this.textBox_rusName = new System.Windows.Forms.TextBox();
            this.textBox_description = new System.Windows.Forms.TextBox();
            this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.comboBox_role = new System.Windows.Forms.ComboBox();
            this.comboBox_status = new System.Windows.Forms.ComboBox();
            this.button_OK = new System.Windows.Forms.Button();
            this.button_cancel = new System.Windows.Forms.Button();
            this.label_ = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBox_podcast = new System.Windows.Forms.ComboBox();
            this.comboBox_season = new System.Windows.Forms.ComboBox();
            this.comboBox_episode = new System.Windows.Forms.ComboBox();
            this.textBox_duration = new System.Windows.Forms.TextBox();
            this.button_newEpisode = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // richTextBox_eng
            // 
            this.richTextBox_eng.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.richTextBox_eng.Location = new System.Drawing.Point(16, 254);
            this.richTextBox_eng.Name = "richTextBox_eng";
            this.richTextBox_eng.Size = new System.Drawing.Size(383, 452);
            this.richTextBox_eng.TabIndex = 0;
            this.richTextBox_eng.Text = "";
            // 
            // richTextBox_rus
            // 
            this.richTextBox_rus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.richTextBox_rus.Location = new System.Drawing.Point(414, 254);
            this.richTextBox_rus.Name = "richTextBox_rus";
            this.richTextBox_rus.Size = new System.Drawing.Size(383, 452);
            this.richTextBox_rus.TabIndex = 1;
            this.richTextBox_rus.Text = "";
            // 
            // textBox_number
            // 
            this.textBox_number.Location = new System.Drawing.Point(134, 99);
            this.textBox_number.Name = "textBox_number";
            this.textBox_number.Size = new System.Drawing.Size(66, 20);
            this.textBox_number.TabIndex = 2;
            // 
            // textBox_engName
            // 
            this.textBox_engName.Location = new System.Drawing.Point(181, 228);
            this.textBox_engName.Name = "textBox_engName";
            this.textBox_engName.Size = new System.Drawing.Size(218, 20);
            this.textBox_engName.TabIndex = 3;
            // 
            // textBox_rusName
            // 
            this.textBox_rusName.Location = new System.Drawing.Point(553, 228);
            this.textBox_rusName.Name = "textBox_rusName";
            this.textBox_rusName.Size = new System.Drawing.Size(244, 20);
            this.textBox_rusName.TabIndex = 4;
            // 
            // textBox_description
            // 
            this.textBox_description.Location = new System.Drawing.Point(92, 137);
            this.textBox_description.Multiline = true;
            this.textBox_description.Name = "textBox_description";
            this.textBox_description.Size = new System.Drawing.Size(440, 72);
            this.textBox_description.TabIndex = 5;
            this.textBox_description.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            // 
            // dateTimePicker
            // 
            this.dateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker.Location = new System.Drawing.Point(677, 137);
            this.dateTimePicker.Name = "dateTimePicker";
            this.dateTimePicker.Size = new System.Drawing.Size(121, 20);
            this.dateTimePicker.TabIndex = 6;
            // 
            // comboBox_role
            // 
            this.comboBox_role.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_role.FormattingEnabled = true;
            this.comboBox_role.Items.AddRange(new object[] {
            "",
            "Переводчик",
            "Редактор"});
            this.comboBox_role.Location = new System.Drawing.Point(677, 190);
            this.comboBox_role.Name = "comboBox_role";
            this.comboBox_role.Size = new System.Drawing.Size(121, 21);
            this.comboBox_role.TabIndex = 7;
            // 
            // comboBox_status
            // 
            this.comboBox_status.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_status.FormattingEnabled = true;
            this.comboBox_status.Location = new System.Drawing.Point(677, 163);
            this.comboBox_status.Name = "comboBox_status";
            this.comboBox_status.Size = new System.Drawing.Size(121, 21);
            this.comboBox_status.TabIndex = 8;
            // 
            // button_OK
            // 
            this.button_OK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_OK.BackColor = System.Drawing.Color.Transparent;
            this.button_OK.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(175)))), ((int)(((byte)(211)))));
            this.button_OK.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button_OK.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(175)))), ((int)(((byte)(211)))));
            this.button_OK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_OK.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_OK.Location = new System.Drawing.Point(684, 724);
            this.button_OK.Name = "button_OK";
            this.button_OK.Size = new System.Drawing.Size(109, 26);
            this.button_OK.TabIndex = 35;
            this.button_OK.Text = "Подтверить";
            this.button_OK.UseVisualStyleBackColor = false;
            this.button_OK.Click += new System.EventHandler(this.button_OK_Click);
            // 
            // button_cancel
            // 
            this.button_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_cancel.BackColor = System.Drawing.Color.Transparent;
            this.button_cancel.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(175)))), ((int)(((byte)(211)))));
            this.button_cancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button_cancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(175)))), ((int)(((byte)(211)))));
            this.button_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_cancel.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_cancel.Location = new System.Drawing.Point(569, 724);
            this.button_cancel.Name = "button_cancel";
            this.button_cancel.Size = new System.Drawing.Size(109, 29);
            this.button_cancel.TabIndex = 36;
            this.button_cancel.Text = "Назад";
            this.button_cancel.UseVisualStyleBackColor = false;
            this.button_cancel.Click += new System.EventHandler(this.button_cancel_Click);
            // 
            // label_
            // 
            this.label_.AutoSize = true;
            this.label_.BackColor = System.Drawing.Color.Transparent;
            this.label_.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_.Location = new System.Drawing.Point(12, 100);
            this.label_.Name = "label_";
            this.label_.Size = new System.Drawing.Size(116, 18);
            this.label_.TabIndex = 37;
            this.label_.Text = "Номер эпизода:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 228);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(159, 18);
            this.label1.TabIndex = 38;
            this.label1.Text = "Английское название:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(411, 228);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 18);
            this.label2.TabIndex = 39;
            this.label2.Text = "Русское название:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 137);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 18);
            this.label3.TabIndex = 40;
            this.label3.Text = "Описание:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(538, 137);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 18);
            this.label4.TabIndex = 41;
            this.label4.Text = "Дата выхода:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(538, 164);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(128, 18);
            this.label6.TabIndex = 42;
            this.label6.Text = "Статус перевода:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(538, 191);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 18);
            this.label7.TabIndex = 43;
            this.label7.Text = "Ваша роль:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(323, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 18);
            this.label5.TabIndex = 47;
            this.label5.Text = "Сезон:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(102, 13);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(69, 18);
            this.label8.TabIndex = 46;
            this.label8.Text = "Подкаст:";
            // 
            // comboBox_podcast
            // 
            this.comboBox_podcast.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_podcast.FormattingEnabled = true;
            this.comboBox_podcast.Location = new System.Drawing.Point(177, 12);
            this.comboBox_podcast.Name = "comboBox_podcast";
            this.comboBox_podcast.Size = new System.Drawing.Size(121, 21);
            this.comboBox_podcast.TabIndex = 45;
            this.comboBox_podcast.SelectedIndexChanged += new System.EventHandler(this.comboBox_podcast_SelectedIndexChanged);
            // 
            // comboBox_season
            // 
            this.comboBox_season.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_season.FormattingEnabled = true;
            this.comboBox_season.Location = new System.Drawing.Point(382, 12);
            this.comboBox_season.Name = "comboBox_season";
            this.comboBox_season.Size = new System.Drawing.Size(121, 21);
            this.comboBox_season.TabIndex = 44;
            this.comboBox_season.SelectedIndexChanged += new System.EventHandler(this.comboBox_season_SelectedIndexChanged);
            // 
            // comboBox_episode
            // 
            this.comboBox_episode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_episode.FormattingEnabled = true;
            this.comboBox_episode.Location = new System.Drawing.Point(517, 13);
            this.comboBox_episode.Name = "comboBox_episode";
            this.comboBox_episode.Size = new System.Drawing.Size(121, 21);
            this.comboBox_episode.TabIndex = 48;
            this.comboBox_episode.SelectedIndexChanged += new System.EventHandler(this.comboBox_episode_SelectedIndexChanged);
            // 
            // textBox_duration
            // 
            this.textBox_duration.Location = new System.Drawing.Point(342, 99);
            this.textBox_duration.Name = "textBox_duration";
            this.textBox_duration.Size = new System.Drawing.Size(66, 20);
            this.textBox_duration.TabIndex = 49;
            // 
            // button_newEpisode
            // 
            this.button_newEpisode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_newEpisode.BackColor = System.Drawing.Color.Transparent;
            this.button_newEpisode.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(175)))), ((int)(((byte)(211)))));
            this.button_newEpisode.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button_newEpisode.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(175)))), ((int)(((byte)(211)))));
            this.button_newEpisode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_newEpisode.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_newEpisode.Location = new System.Drawing.Point(677, 13);
            this.button_newEpisode.Name = "button_newEpisode";
            this.button_newEpisode.Size = new System.Drawing.Size(109, 29);
            this.button_newEpisode.TabIndex = 50;
            this.button_newEpisode.Text = "Новый";
            this.button_newEpisode.UseVisualStyleBackColor = false;
            this.button_newEpisode.Click += new System.EventHandler(this.button_newEpisode_Click);
            // 
            // frmAddEpisode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackgroundImage = global::PodcastManagement.Properties.Resources.background;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(805, 757);
            this.Controls.Add(this.button_newEpisode);
            this.Controls.Add(this.textBox_duration);
            this.Controls.Add(this.comboBox_episode);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.comboBox_podcast);
            this.Controls.Add(this.comboBox_season);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label_);
            this.Controls.Add(this.button_cancel);
            this.Controls.Add(this.button_OK);
            this.Controls.Add(this.comboBox_status);
            this.Controls.Add(this.comboBox_role);
            this.Controls.Add(this.dateTimePicker);
            this.Controls.Add(this.textBox_description);
            this.Controls.Add(this.textBox_rusName);
            this.Controls.Add(this.textBox_engName);
            this.Controls.Add(this.textBox_number);
            this.Controls.Add(this.richTextBox_rus);
            this.Controls.Add(this.richTextBox_eng);
            this.Name = "frmAddEpisode";
            this.Text = "Добавление эпизода";
            this.Load += new System.EventHandler(this.frmAddEpisode_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox_eng;
        private System.Windows.Forms.RichTextBox richTextBox_rus;
        private System.Windows.Forms.TextBox textBox_number;
        private System.Windows.Forms.TextBox textBox_engName;
        private System.Windows.Forms.TextBox textBox_rusName;
        private System.Windows.Forms.TextBox textBox_description;
        private System.Windows.Forms.DateTimePicker dateTimePicker;
        private System.Windows.Forms.ComboBox comboBox_role;
        private System.Windows.Forms.ComboBox comboBox_status;
        private System.Windows.Forms.Button button_OK;
        private System.Windows.Forms.Button button_cancel;
        private System.Windows.Forms.Label label_;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboBox_podcast;
        private System.Windows.Forms.ComboBox comboBox_season;
        private System.Windows.Forms.ComboBox comboBox_episode;
        private System.Windows.Forms.TextBox textBox_duration;
        private System.Windows.Forms.Button button_newEpisode;
    }
}