﻿namespace PodcastManagement.Admin
{
    partial class frmAdminMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonBack = new System.Windows.Forms.Button();
            this.button_podcastControl = new System.Windows.Forms.Button();
            this.button_newsControl = new System.Windows.Forms.Button();
            this.button_statusControl = new System.Windows.Forms.Button();
            this.button_userControl = new System.Windows.Forms.Button();
            this.groupBox_login = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel_auth = new System.Windows.Forms.FlowLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox_login.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonBack
            // 
            this.buttonBack.BackColor = System.Drawing.Color.Transparent;
            this.buttonBack.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(175)))), ((int)(((byte)(211)))));
            this.buttonBack.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonBack.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(217)))), ((int)(((byte)(234)))));
            this.buttonBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBack.Font = new System.Drawing.Font("Panton", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonBack.Location = new System.Drawing.Point(12, 220);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(119, 31);
            this.buttonBack.TabIndex = 7;
            this.buttonBack.Text = "Назад";
            this.buttonBack.UseVisualStyleBackColor = false;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // button_podcastControl
            // 
            this.button_podcastControl.BackColor = System.Drawing.Color.Transparent;
            this.button_podcastControl.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(175)))), ((int)(((byte)(211)))));
            this.button_podcastControl.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button_podcastControl.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(217)))), ((int)(((byte)(234)))));
            this.button_podcastControl.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_podcastControl.Font = new System.Drawing.Font("Panton", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_podcastControl.Location = new System.Drawing.Point(6, 36);
            this.button_podcastControl.Name = "button_podcastControl";
            this.button_podcastControl.Size = new System.Drawing.Size(235, 31);
            this.button_podcastControl.TabIndex = 8;
            this.button_podcastControl.Text = "Управление подкастами";
            this.button_podcastControl.UseVisualStyleBackColor = false;
            // 
            // button_newsControl
            // 
            this.button_newsControl.BackColor = System.Drawing.Color.Transparent;
            this.button_newsControl.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(175)))), ((int)(((byte)(211)))));
            this.button_newsControl.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button_newsControl.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(217)))), ((int)(((byte)(234)))));
            this.button_newsControl.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_newsControl.Font = new System.Drawing.Font("Panton", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_newsControl.Location = new System.Drawing.Point(6, 73);
            this.button_newsControl.Name = "button_newsControl";
            this.button_newsControl.Size = new System.Drawing.Size(235, 31);
            this.button_newsControl.TabIndex = 9;
            this.button_newsControl.Text = "Управление новостями";
            this.button_newsControl.UseVisualStyleBackColor = false;
            // 
            // button_statusControl
            // 
            this.button_statusControl.BackColor = System.Drawing.Color.Transparent;
            this.button_statusControl.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(175)))), ((int)(((byte)(211)))));
            this.button_statusControl.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button_statusControl.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(217)))), ((int)(((byte)(234)))));
            this.button_statusControl.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_statusControl.Font = new System.Drawing.Font("Panton", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_statusControl.Location = new System.Drawing.Point(6, 110);
            this.button_statusControl.Name = "button_statusControl";
            this.button_statusControl.Size = new System.Drawing.Size(235, 31);
            this.button_statusControl.TabIndex = 10;
            this.button_statusControl.Text = "Управление статусами";
            this.button_statusControl.UseVisualStyleBackColor = false;
            this.button_statusControl.Click += new System.EventHandler(this.button_statusControl_Click);
            // 
            // button_userControl
            // 
            this.button_userControl.BackColor = System.Drawing.Color.Transparent;
            this.button_userControl.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(175)))), ((int)(((byte)(211)))));
            this.button_userControl.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button_userControl.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(217)))), ((int)(((byte)(234)))));
            this.button_userControl.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_userControl.Font = new System.Drawing.Font("Panton", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_userControl.Location = new System.Drawing.Point(6, 148);
            this.button_userControl.Name = "button_userControl";
            this.button_userControl.Size = new System.Drawing.Size(235, 31);
            this.button_userControl.TabIndex = 11;
            this.button_userControl.Text = "Управление пользователями";
            this.button_userControl.UseVisualStyleBackColor = false;
            // 
            // groupBox_login
            // 
            this.groupBox_login.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox_login.BackColor = System.Drawing.Color.Transparent;
            this.groupBox_login.Controls.Add(this.flowLayoutPanel_auth);
            this.groupBox_login.Font = new System.Drawing.Font("BigNoodleTitlingCyr", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox_login.Location = new System.Drawing.Point(301, 12);
            this.groupBox_login.Name = "groupBox_login";
            this.groupBox_login.Size = new System.Drawing.Size(258, 202);
            this.groupBox_login.TabIndex = 12;
            this.groupBox_login.TabStop = false;
            this.groupBox_login.Text = "Личный Кабинет";
            // 
            // flowLayoutPanel_auth
            // 
            this.flowLayoutPanel_auth.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel_auth.Location = new System.Drawing.Point(3, 33);
            this.flowLayoutPanel_auth.Name = "flowLayoutPanel_auth";
            this.flowLayoutPanel_auth.Size = new System.Drawing.Size(252, 166);
            this.flowLayoutPanel_auth.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.button_podcastControl);
            this.groupBox1.Controls.Add(this.button_newsControl);
            this.groupBox1.Controls.Add(this.button_userControl);
            this.groupBox1.Controls.Add(this.button_statusControl);
            this.groupBox1.Font = new System.Drawing.Font("BigNoodleTitlingCyr", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(258, 202);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Панель управления";
            // 
            // frmAdminMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::PodcastManagement.Properties.Resources.background;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(571, 263);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox_login);
            this.Controls.Add(this.buttonBack);
            this.DoubleBuffered = true;
            this.Name = "frmAdminMenu";
            this.Text = "Управление";
            this.Load += new System.EventHandler(this.frmAdminMenu_Load);
            this.groupBox_login.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonBack;
        private System.Windows.Forms.Button button_podcastControl;
        private System.Windows.Forms.Button button_newsControl;
        private System.Windows.Forms.Button button_statusControl;
        private System.Windows.Forms.Button button_userControl;
        private System.Windows.Forms.GroupBox groupBox_login;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel_auth;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}