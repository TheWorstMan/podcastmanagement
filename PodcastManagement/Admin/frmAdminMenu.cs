﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PodcastManagement.Admin
{
    public partial class frmAdminMenu : Form
    {
        public frmAdminMenu()
        {
            InitializeComponent();
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            Utility.OpenForm(new Profile.frmProfile(), this);
        }

        private void frmAdminMenu_Load(object sender, EventArgs e)
        {
            foreach (var c in Utility.getUProfile(this))
                flowLayoutPanel_auth.Controls.Add(c);
        }

        private void button_statusControl_Click(object sender, EventArgs e)
        {
            Utility.OpenForm(new Admin.frmStatusControl(), this);
        }
    }
}
