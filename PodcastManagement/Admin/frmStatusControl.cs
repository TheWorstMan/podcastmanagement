﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PodcastManagement.Admin
{
    public partial class frmStatusControl : Form
    {
        private BindingList<Status> status;
        private Status temp;

        public frmStatusControl()
        {
            InitializeComponent();
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            Utility.OpenForm(new Admin.frmAdminMenu(), this);
        }

        private void frmStatusControl_Load(object sender, EventArgs e)
        {
            foreach (var c in Utility.getUProfile(this))
                flowLayoutPanel_auth.Controls.Add(c);

            status = new BindingList<Status>(Utility.dBase.Status.ToList());

            dataGridView.AllowUserToAddRows = true;
            dataGridView.AllowUserToDeleteRows = true;
            dataGridView.AutoGenerateColumns = false;
            dataGridView.DataSource = status;
        }

        private void dataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            Status s = dataGridView.Rows[e.RowIndex].DataBoundItem as Status;

            try
            {
                Utility.dBase.Status.Add(s);
                Utility.dBase.SaveChanges();

                temp = null;
            }
            catch
            {
                Utility.dBase.Status.Remove(s);

                if (temp != null)
                {
                    s.StatusName = temp.StatusName;
                    Utility.dBase.Status.Add(s);
                    Utility.dBase.SaveChanges();
                }
                else
                    status.RemoveAt(e.RowIndex);

                temp = null;

                MessageBox.Show(null, "Невозможно добавить статус. Такой статус уже существует.", "Ошибка при изменении", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void dataGridView_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            if (e.Row.Index < 0)
                return;

            try
            {
                Status s = dataGridView.Rows[e.Row.Index].DataBoundItem as Status;

                Utility.dBase.Status.Remove(s);
                Utility.dBase.SaveChanges();
            }
            catch
            {
                e.Cancel = true;
                MessageBox.Show(null, "Невозможно удалить статус. Статус используется другими объектами.", "Ошибка при удалении", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void dataGridView_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        {

        }

        private void dataGridView_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {

        }

        private void dataGridView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            Status s = dataGridView.Rows[e.RowIndex].DataBoundItem as Status;

            try
            {
                if (s == null)
                    return;

                temp = s.Clone() as Status;

                Utility.dBase.Status.Remove(s);
                Utility.dBase.SaveChanges();
            }
            catch
            {
                e.Cancel = true;
                Utility.dBase.Status.Add(s);
                MessageBox.Show(null, "Невозможно удалить статус. Статус используется другими объектами.", "Ошибка при удалении", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
