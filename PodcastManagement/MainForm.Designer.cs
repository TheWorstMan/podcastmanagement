﻿namespace PodcastManagement
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.groupBox_podcast = new System.Windows.Forms.GroupBox();
            this.button_navigation = new System.Windows.Forms.Button();
            this.flowLayoutPanel_podcast = new System.Windows.Forms.FlowLayoutPanel();
            this.groupBox_news = new System.Windows.Forms.GroupBox();
            this.button_news = new System.Windows.Forms.Button();
            this.flowLayoutPanel_news = new System.Windows.Forms.FlowLayoutPanel();
            this.groupBox_login = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel_auth = new System.Windows.Forms.FlowLayoutPanel();
            this.labelLogin = new System.Windows.Forms.Label();
            this.textBoxLogin = new System.Windows.Forms.TextBox();
            this.labelPassword = new System.Windows.Forms.Label();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.buttonLogin = new System.Windows.Forms.Button();
            this.buttonRegistration = new System.Windows.Forms.Button();
            this.groupBox_lastEpisode = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel_episode = new System.Windows.Forms.FlowLayoutPanel();
            this.label_title = new System.Windows.Forms.Label();
            this.label_content = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.label_time = new System.Windows.Forms.Label();
            this.groupBox_podcast.SuspendLayout();
            this.groupBox_news.SuspendLayout();
            this.groupBox_login.SuspendLayout();
            this.flowLayoutPanel_auth.SuspendLayout();
            this.groupBox_lastEpisode.SuspendLayout();
            this.flowLayoutPanel_episode.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox_podcast
            // 
            this.groupBox_podcast.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox_podcast.BackColor = System.Drawing.Color.Transparent;
            this.groupBox_podcast.Controls.Add(this.button_navigation);
            this.groupBox_podcast.Controls.Add(this.flowLayoutPanel_podcast);
            this.groupBox_podcast.Font = new System.Drawing.Font("BigNoodleTitlingCyr", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox_podcast.Location = new System.Drawing.Point(585, 12);
            this.groupBox_podcast.Name = "groupBox_podcast";
            this.groupBox_podcast.Size = new System.Drawing.Size(258, 294);
            this.groupBox_podcast.TabIndex = 1;
            this.groupBox_podcast.TabStop = false;
            this.groupBox_podcast.Text = "Подкасты";
            // 
            // button_navigation
            // 
            this.button_navigation.AutoSize = true;
            this.button_navigation.BackColor = System.Drawing.Color.White;
            this.button_navigation.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(175)))), ((int)(((byte)(211)))));
            this.button_navigation.FlatAppearance.MouseDownBackColor = System.Drawing.Color.GhostWhite;
            this.button_navigation.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(217)))), ((int)(((byte)(234)))));
            this.button_navigation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_navigation.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_navigation.Location = new System.Drawing.Point(114, 0);
            this.button_navigation.Name = "button_navigation";
            this.button_navigation.Size = new System.Drawing.Size(144, 30);
            this.button_navigation.TabIndex = 3;
            this.button_navigation.Text = "Навигация";
            this.button_navigation.UseVisualStyleBackColor = false;
            this.button_navigation.Click += new System.EventHandler(this.button_navigation_Click);
            // 
            // flowLayoutPanel_podcast
            // 
            this.flowLayoutPanel_podcast.AutoScroll = true;
            this.flowLayoutPanel_podcast.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel_podcast.Location = new System.Drawing.Point(3, 33);
            this.flowLayoutPanel_podcast.Name = "flowLayoutPanel_podcast";
            this.flowLayoutPanel_podcast.Size = new System.Drawing.Size(252, 258);
            this.flowLayoutPanel_podcast.TabIndex = 2;
            // 
            // groupBox_news
            // 
            this.groupBox_news.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox_news.BackColor = System.Drawing.Color.Transparent;
            this.groupBox_news.Controls.Add(this.button_news);
            this.groupBox_news.Controls.Add(this.flowLayoutPanel_news);
            this.groupBox_news.Font = new System.Drawing.Font("BigNoodleTitlingCyr", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox_news.Location = new System.Drawing.Point(585, 312);
            this.groupBox_news.Name = "groupBox_news";
            this.groupBox_news.Size = new System.Drawing.Size(258, 124);
            this.groupBox_news.TabIndex = 3;
            this.groupBox_news.TabStop = false;
            this.groupBox_news.Text = "новости";
            // 
            // button_news
            // 
            this.button_news.AutoSize = true;
            this.button_news.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(234)))), ((int)(((byte)(240)))));
            this.button_news.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(175)))), ((int)(((byte)(211)))));
            this.button_news.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(234)))), ((int)(((byte)(240)))));
            this.button_news.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(217)))), ((int)(((byte)(234)))));
            this.button_news.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_news.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_news.Location = new System.Drawing.Point(114, 0);
            this.button_news.Name = "button_news";
            this.button_news.Size = new System.Drawing.Size(144, 30);
            this.button_news.TabIndex = 4;
            this.button_news.Text = "Все новости";
            this.button_news.UseVisualStyleBackColor = false;
            this.button_news.Click += new System.EventHandler(this.button_news_Click);
            // 
            // flowLayoutPanel_news
            // 
            this.flowLayoutPanel_news.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel_news.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel_news.Location = new System.Drawing.Point(3, 33);
            this.flowLayoutPanel_news.Name = "flowLayoutPanel_news";
            this.flowLayoutPanel_news.Size = new System.Drawing.Size(252, 88);
            this.flowLayoutPanel_news.TabIndex = 2;
            // 
            // groupBox_login
            // 
            this.groupBox_login.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox_login.BackColor = System.Drawing.Color.Transparent;
            this.groupBox_login.Controls.Add(this.flowLayoutPanel_auth);
            this.groupBox_login.Font = new System.Drawing.Font("BigNoodleTitlingCyr", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox_login.Location = new System.Drawing.Point(585, 442);
            this.groupBox_login.Name = "groupBox_login";
            this.groupBox_login.Size = new System.Drawing.Size(258, 185);
            this.groupBox_login.TabIndex = 4;
            this.groupBox_login.TabStop = false;
            this.groupBox_login.Text = "Авторизация";
            // 
            // flowLayoutPanel_auth
            // 
            this.flowLayoutPanel_auth.Controls.Add(this.labelLogin);
            this.flowLayoutPanel_auth.Controls.Add(this.textBoxLogin);
            this.flowLayoutPanel_auth.Controls.Add(this.labelPassword);
            this.flowLayoutPanel_auth.Controls.Add(this.textBoxPassword);
            this.flowLayoutPanel_auth.Controls.Add(this.buttonLogin);
            this.flowLayoutPanel_auth.Controls.Add(this.buttonRegistration);
            this.flowLayoutPanel_auth.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel_auth.Location = new System.Drawing.Point(3, 33);
            this.flowLayoutPanel_auth.Name = "flowLayoutPanel_auth";
            this.flowLayoutPanel_auth.Size = new System.Drawing.Size(252, 149);
            this.flowLayoutPanel_auth.TabIndex = 2;
            // 
            // labelLogin
            // 
            this.labelLogin.Font = new System.Drawing.Font("Panton", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLogin.Location = new System.Drawing.Point(3, 0);
            this.labelLogin.Name = "labelLogin";
            this.labelLogin.Size = new System.Drawing.Size(96, 30);
            this.labelLogin.TabIndex = 0;
            this.labelLogin.Text = "Логин";
            this.labelLogin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxLogin
            // 
            this.textBoxLogin.Font = new System.Drawing.Font("Panton SemiBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxLogin.Location = new System.Drawing.Point(105, 3);
            this.textBoxLogin.Name = "textBoxLogin";
            this.textBoxLogin.Size = new System.Drawing.Size(144, 27);
            this.textBoxLogin.TabIndex = 1;
            // 
            // labelPassword
            // 
            this.labelPassword.Font = new System.Drawing.Font("Panton", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelPassword.Location = new System.Drawing.Point(3, 33);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(96, 30);
            this.labelPassword.TabIndex = 2;
            this.labelPassword.Text = "Пароль";
            this.labelPassword.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Font = new System.Drawing.Font("Panton SemiBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxPassword.Location = new System.Drawing.Point(105, 36);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.PasswordChar = '*';
            this.textBoxPassword.Size = new System.Drawing.Size(144, 27);
            this.textBoxPassword.TabIndex = 3;
            // 
            // buttonLogin
            // 
            this.buttonLogin.BackColor = System.Drawing.Color.Transparent;
            this.buttonLogin.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(175)))), ((int)(((byte)(211)))));
            this.buttonLogin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonLogin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(217)))), ((int)(((byte)(234)))));
            this.buttonLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLogin.Font = new System.Drawing.Font("Panton", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonLogin.Location = new System.Drawing.Point(3, 69);
            this.buttonLogin.Name = "buttonLogin";
            this.buttonLogin.Size = new System.Drawing.Size(119, 31);
            this.buttonLogin.TabIndex = 5;
            this.buttonLogin.Text = "Войти";
            this.buttonLogin.UseVisualStyleBackColor = false;
            this.buttonLogin.Click += new System.EventHandler(this.buttonLogin_Click);
            // 
            // buttonRegistration
            // 
            this.buttonRegistration.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(175)))), ((int)(((byte)(211)))));
            this.buttonRegistration.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonRegistration.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(217)))), ((int)(((byte)(234)))));
            this.buttonRegistration.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRegistration.Font = new System.Drawing.Font("Panton", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonRegistration.Location = new System.Drawing.Point(128, 69);
            this.buttonRegistration.Name = "buttonRegistration";
            this.buttonRegistration.Size = new System.Drawing.Size(121, 31);
            this.buttonRegistration.TabIndex = 4;
            this.buttonRegistration.Text = "Регистрация";
            this.buttonRegistration.UseVisualStyleBackColor = true;
            this.buttonRegistration.Click += new System.EventHandler(this.buttonRegistration_Click);
            // 
            // groupBox_lastEpisode
            // 
            this.groupBox_lastEpisode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox_lastEpisode.BackColor = System.Drawing.Color.Transparent;
            this.groupBox_lastEpisode.Controls.Add(this.flowLayoutPanel_episode);
            this.groupBox_lastEpisode.Font = new System.Drawing.Font("BigNoodleTitlingCyr", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox_lastEpisode.Location = new System.Drawing.Point(12, 12);
            this.groupBox_lastEpisode.Name = "groupBox_lastEpisode";
            this.groupBox_lastEpisode.Size = new System.Drawing.Size(549, 612);
            this.groupBox_lastEpisode.TabIndex = 3;
            this.groupBox_lastEpisode.TabStop = false;
            this.groupBox_lastEpisode.Text = "последний эпизод";
            // 
            // flowLayoutPanel_episode
            // 
            this.flowLayoutPanel_episode.Controls.Add(this.label_title);
            this.flowLayoutPanel_episode.Controls.Add(this.label_content);
            this.flowLayoutPanel_episode.Controls.Add(this.linkLabel1);
            this.flowLayoutPanel_episode.Controls.Add(this.label_time);
            this.flowLayoutPanel_episode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel_episode.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel_episode.Location = new System.Drawing.Point(3, 33);
            this.flowLayoutPanel_episode.Name = "flowLayoutPanel_episode";
            this.flowLayoutPanel_episode.Size = new System.Drawing.Size(543, 576);
            this.flowLayoutPanel_episode.TabIndex = 2;
            // 
            // label_title
            // 
            this.label_title.Font = new System.Drawing.Font("Panton", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_title.Location = new System.Drawing.Point(3, 0);
            this.label_title.Name = "label_title";
            this.label_title.Size = new System.Drawing.Size(537, 36);
            this.label_title.TabIndex = 0;
            this.label_title.Text = "label_title";
            this.label_title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_content
            // 
            this.label_content.Font = new System.Drawing.Font("Panton", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_content.Location = new System.Drawing.Point(3, 36);
            this.label_content.Name = "label_content";
            this.label_content.Size = new System.Drawing.Size(537, 494);
            this.label_content.TabIndex = 1;
            this.label_content.Text = "label1ggggggggggggggggggggggggggggggggggggggggggggggggg";
            // 
            // linkLabel1
            // 
            this.linkLabel1.Font = new System.Drawing.Font("Panton", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel1.Location = new System.Drawing.Point(3, 530);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(537, 20);
            this.linkLabel1.TabIndex = 3;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Читать далее...";
            this.linkLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // label_time
            // 
            this.label_time.AutoSize = true;
            this.label_time.Font = new System.Drawing.Font("Panton", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_time.Location = new System.Drawing.Point(3, 550);
            this.label_time.Name = "label_time";
            this.label_time.Size = new System.Drawing.Size(47, 19);
            this.label_time.TabIndex = 2;
            this.label_time.Text = "label1";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackgroundImage = global::PodcastManagement.Properties.Resources.background;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(855, 636);
            this.Controls.Add(this.groupBox_news);
            this.Controls.Add(this.groupBox_lastEpisode);
            this.Controls.Add(this.groupBox_login);
            this.Controls.Add(this.groupBox_podcast);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "Подкасты";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.MainForm_Paint);
            this.groupBox_podcast.ResumeLayout(false);
            this.groupBox_podcast.PerformLayout();
            this.groupBox_news.ResumeLayout(false);
            this.groupBox_news.PerformLayout();
            this.groupBox_login.ResumeLayout(false);
            this.flowLayoutPanel_auth.ResumeLayout(false);
            this.flowLayoutPanel_auth.PerformLayout();
            this.groupBox_lastEpisode.ResumeLayout(false);
            this.flowLayoutPanel_episode.ResumeLayout(false);
            this.flowLayoutPanel_episode.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox_podcast;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel_podcast;
        private System.Windows.Forms.GroupBox groupBox_news;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel_news;
        private System.Windows.Forms.GroupBox groupBox_login;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel_auth;
        private System.Windows.Forms.GroupBox groupBox_lastEpisode;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel_episode;
        private System.Windows.Forms.Label label_title;
        private System.Windows.Forms.Label label_content;
        private System.Windows.Forms.Label label_time;
        private System.Windows.Forms.Label labelLogin;
        private System.Windows.Forms.TextBox textBoxLogin;
        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.Button buttonRegistration;
        private System.Windows.Forms.Button buttonLogin;
        private System.Windows.Forms.Button button_navigation;
        private System.Windows.Forms.Button button_news;
        private System.Windows.Forms.LinkLabel linkLabel1;
    }
}

