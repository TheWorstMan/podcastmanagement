﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PodcastManagement
{
    public static class Utility
    {
        private static PodcastManagementEntities database;
        public static User current_user = null;
        public static PodcastManagementEntities dBase
        {
            get
            {
                if (database == null)
                    database = new PodcastManagementEntities();
                return database;
            }
        }

        public static void CloseConnection()
        {
            if(database != null)
                database.Database.Connection.Close();
        }

        public static void OpenForm(Form frm, Form owner, bool close = true)
        {
            if (close)
            {
                frm.Show();

                owner.Close();
            }
            else
            {
                frm.ShowDialog();
            }
        }

        public static bool Auth(string login, string password)
        {
            try
            {
                var result = dBase.User.SingleOrDefault(u => u.UserLogin == login && u.UserPassword == password);

                if (result != null)
                    current_user = result;

                return current_user != null;
            }
            catch
            {
                return false;
            }
        }

        public static IEnumerable<Control> getNews(int count)
        {
            foreach (var n in dBase.News.OrderByDescending(n => n.NewsDate).Take(count))
            {
                Label lbl = new Label();
                lbl.AutoSize = true;
                lbl.Font = new System.Drawing.Font("Panton", 12);
                lbl.Text = string.Format("{0} - {1}", n.NewsArticle, n.NewsDate.ToString("dd.MM.yyyy"));

                yield return lbl;
            }
        }

        public static IEnumerable<Control> getAllNews()
        {
            foreach (var n in dBase.News.Include("User").OrderByDescending(n => n.NewsDate))
            {
                Label lbl = new Label();
                lbl.AutoSize = true;
                //lbl.Dock = DockStyle.Fill;
                lbl.Font = new System.Drawing.Font("Panton", 22);
                lbl.Text = n.NewsArticle;

                yield return lbl;

                //Label none = new Label();
                //none.Size = new Size(560, 5);

                //yield return none;

                Label lbl2 = new Label();
                lbl2.AutoSize = true;
                lbl2.Margin = new Padding(10, 10, 0, 0);
                //lbl.Dock = DockStyle.Fill;
                lbl2.Font = new System.Drawing.Font("Panton", 12);
                lbl2.Text = string.Format("{0}", n.NewsText);

                yield return lbl2;

                Label none2 = new Label();
                none2.AutoSize = true;
                none2.Font = new System.Drawing.Font("Panton", 10);
                none2.Text = string.Format("Автор: {0} \t Дата: {1}", n.User.UserLogin.ToString(), n.NewsDate.ToString("dd.MM.yyyy"));
                none2.Margin = new Padding(5, 10, 0, 10);

                yield return none2;

            }
        }

        public static IEnumerable<Control> getUProfile(Form frm)
        {
            FlowLayoutPanel flp_image = new FlowLayoutPanel();
            
            flp_image.AutoSize = false;
            flp_image.Width = 100;

            PictureBox pb = new PictureBox();

            if(current_user.UserAvatar != null)
            {
                using (var ms = new MemoryStream(current_user.UserAvatar))
                    pb.Image = Bitmap.FromStream(ms);
            }
            else            
                pb.Image = Properties.Resources.userIcon;

            pb.Width = 100;
            pb.Height = 100;
            pb.SizeMode = PictureBoxSizeMode.Zoom;

            flp_image.Controls.Add(pb);

            yield return flp_image;

            FlowLayoutPanel flp_text = new FlowLayoutPanel();
            flp_text.AutoSize = false;
            flp_text.Width = 140;
            flp_text.Height = 150;
            

            Label lbl_login = new Label();
            lbl_login.Text = current_user.UserLogin;
            lbl_login.AutoSize = true;
            lbl_login.Font = new Font("Panton", 11);


            Label lbl_role = new Label();
            lbl_role.AutoSize = true;
            lbl_role.Font = new Font("Panton", 9);
            lbl_role.Text = string.Format("Роль: {0}", current_user.UserRole);

            Button btn_profile = new Button();
            btn_profile.Text = "Профиль";
            btn_profile.Font = new Font("Panton", 11);
            btn_profile.AutoSize = true;
            btn_profile.Width = 120;
            btn_profile.BackColor = Color.Transparent;
            btn_profile.FlatStyle = FlatStyle.Flat;
            btn_profile.FlatAppearance.BorderColor = Color.FromArgb(209, 175, 211);
            btn_profile.FlatAppearance.MouseDownBackColor = Color.Transparent;
            btn_profile.FlatAppearance.MouseOverBackColor = Color.FromArgb(233, 217, 234);

            btn_profile.Click += delegate
            {
                Utility.OpenForm(new Profile.frmProfile(frm.GetType()), frm);            
            };

            Button btn_exit= new Button();
            btn_exit.Text = "Выход";
            btn_exit.Font = new Font("Panton", 11);
            btn_exit.AutoSize = true;
            btn_exit.Width = 120;
            btn_exit.FlatStyle = FlatStyle.Flat;
            btn_exit.FlatAppearance.BorderColor = Color.FromArgb(209, 175, 211);
            btn_exit.FlatAppearance.MouseDownBackColor = Color.Transparent;
            btn_exit.FlatAppearance.MouseOverBackColor = Color.FromArgb(233, 217, 234);

            btn_exit.Click += delegate
            {
                Utility.current_user = null;
                Utility.OpenForm((Form)Activator.CreateInstance(Assembly.GetExecutingAssembly().GetTypes().Where(a => a.BaseType == typeof(Form) && a.Name == frm.Name).FirstOrDefault()), frm);
            };

            Button btn_fav = new Button();
            btn_fav.Text = "Избранное";
            btn_fav.Font = new Font("Panton", 11);
            btn_fav.AutoSize = true;
            btn_fav.Width = 120;
            btn_fav.FlatStyle = FlatStyle.Flat;
            btn_fav.FlatAppearance.BorderColor = Color.FromArgb(209, 175, 211);
            btn_fav.FlatAppearance.MouseDownBackColor = Color.Transparent;
            btn_fav.FlatAppearance.MouseOverBackColor = Color.FromArgb(233, 217, 234);

            btn_fav.Click += delegate
            {
                Utility.OpenForm(new Profile.frmFavourite(), frm);
            };


            flp_text.Controls.Add(lbl_login);
            flp_text.Controls.Add(lbl_role);
            flp_text.Controls.Add(btn_profile);
            flp_text.Controls.Add(btn_fav);
            flp_text.Controls.Add(btn_exit);

            yield return flp_text;
        }
    }
}
