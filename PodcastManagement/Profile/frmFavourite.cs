﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PodcastManagement.Profile
{
    public partial class frmFavourite : Form
    {
        private Type backFrm;

        public frmFavourite(Type frm = null)
        {
            InitializeComponent();

            this.backFrm = frm;

            foreach(var pr in Utility.getUProfile(this))
            {
                this.flowLayoutPanel_auth.Controls.Add(pr);
            }

            foreach(var p in Utility.current_user.Podcast)
            {
                Panel panel = new Panel();

                panel.AutoSize = true;

                Bitmap bmp;
                using (var mem = new MemoryStream(p.PodcastLogo))
                    bmp = new Bitmap(mem);

                PictureBox pb = new PictureBox();
                pb.Image = bmp;
                pb.Width = 50;
                pb.Height = 50;
                pb.SizeMode = PictureBoxSizeMode.Zoom;

                panel.Controls.Add(pb);

                Label lbl_captionRUS = new Label();
                lbl_captionRUS.AutoSize = true;
                lbl_captionRUS.Text = p.PodcastRussianName;
                lbl_captionRUS.Font = new Font("Panton", 12);
                lbl_captionRUS.Location = new Point(55, 0);

                panel.Controls.Add(lbl_captionRUS);

                Label lbl_captionENG = new Label();
                lbl_captionENG.Text = p.PodcastEnglishName;
                lbl_captionENG.Font = new Font("Panton", 9);
                lbl_captionENG.Location = new Point(55, 20);
                lbl_captionENG.AutoSize = true;

                panel.Controls.Add(lbl_captionENG);

                Label lbl_listened = new Label();
                lbl_listened.Text = "Прослушано эпизодов";
                lbl_listened.Font = new Font("Panton", 8);
                lbl_listened.Location = new Point(5, 55);
                lbl_listened.AutoSize = true;

                panel.Controls.Add(lbl_listened);

                int epCount = p.Season.Sum(s => s.Episode.Count);
                int rdrCount = Utility.current_user.Episode.Where(ep => ep.Season.Podcast == p).Count();

                ProgressBar pg = new ProgressBar();
                pg.Maximum = epCount;
                pg.Value = rdrCount;
                pg.Anchor = AnchorStyles.Left | AnchorStyles.Right;
                pg.Location = new Point(10, 82);
                pg.Width = tableLayoutPanel_podcasts.Width - 80;
                pg.Height = 5;
                pg.Style = ProgressBarStyle.Continuous;
                pg.MarqueeAnimationSpeed = 0;

                panel.Controls.Add(pg);

                Label lbl_episodes = new Label();
                lbl_episodes.Text = string.Format("{0} из {1}", rdrCount, epCount);
                lbl_episodes.Font = new Font("Panton", 9);
                lbl_episodes.Location = new Point(tableLayoutPanel_podcasts.Width - 80, 92);
                lbl_episodes.AutoSize = true;

                panel.Controls.Add(lbl_episodes);

                Button btn_fav = new Button();
                btn_fav.FlatStyle = FlatStyle.Flat;
                btn_fav.FlatAppearance.BorderColor = Color.FromArgb(209, 175, 211);
                btn_fav.FlatAppearance.MouseOverBackColor = Color.FromArgb(233, 217, 234);
                btn_fav.FlatAppearance.BorderSize = 1;
                btn_fav.FlatAppearance.MouseDownBackColor = Color.Transparent;
                btn_fav.BackColor = Color.Transparent;
                btn_fav.Font = new Font("Panton", 12);
                btn_fav.Image = Properties.Resources.favorite_selected;
                btn_fav.Width = 25;
                btn_fav.Height = 25;
                btn_fav.Location = new Point(tableLayoutPanel_podcasts.Width - 55, 0);
                btn_fav.Tag = 1;
                btn_fav.Click += delegate
                {
                    if ((int)btn_fav.Tag == 1)
                    {
                        Utility.current_user.Podcast.Remove(p);
                        btn_fav.Image = Properties.Resources.favorite;
                        btn_fav.Tag = 0;
                    }
                    else
                    {
                        Utility.current_user.Podcast.Add(p);
                        btn_fav.Image = Properties.Resources.favorite_selected;
                        btn_fav.Tag = 1;
                    }

                    Utility.dBase.SaveChanges();
                };

                panel.Controls.Add(btn_fav);

                panel.BackColor = Color.Transparent;

                this.tableLayoutPanel_podcasts.Controls.Add(panel);
            }
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            if (backFrm == null)
            {
                Utility.OpenForm(new MainForm(), this);
                return;
            }

            Utility.OpenForm((Form)Activator.CreateInstance(backFrm), this);
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel_podcasts_Scroll(object sender, ScrollEventArgs e)
        {
            tableLayoutPanel_podcasts.Invalidate();
        }
    }
}
