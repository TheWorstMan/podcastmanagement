﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PodcastManagement.Profile
{
    public partial class frmEditProfile : Form
    {
        private bool ava_change = false;

        public frmEditProfile()
        {
            InitializeComponent();
        }

        private void frmEditProfile_Load(object sender, EventArgs e)
        {
            textBox_email.Text = Utility.current_user.UserEmail;
            label_login.Text = Utility.current_user.UserLogin;
           
            if (Utility.current_user.UserNotify == 1)
                checkBox_notify.Checked = true;
            else
                checkBox_notify.Checked = false;

            if (Utility.current_user.UserAvatar != null)
            {
                pictureBox.BackgroundImage = null;

                using (var ms = new MemoryStream(Utility.current_user.UserAvatar))
                    pictureBox.Image = Bitmap.FromStream(ms);
            }
            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {
            Utility.OpenForm(new Profile.frmProfile(), this);
        }

        private void button_fromFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "*.png;*.jpg;*.jpeg;*.gif;*.bmp|*.png;*.jpg;*.jpeg;*.gif;*.bmp";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    var img = Bitmap.FromFile(ofd.FileName);
                    pictureBox.Image = img;
                    pictureBox.BackgroundImage = null;
                    ava_change = true;
                }
                catch { }
            }
        }

        private void button_OK_Click(object sender, EventArgs e)
        {
            if (textBox_email.Text == Utility.current_user.UserEmail && string.IsNullOrEmpty(textBox_password.Text) 
                && ava_change != true && Convert.ToBoolean(Utility.current_user.UserNotify) == checkBox_notify.Checked)
            {
                MessageBox.Show(null, "Данные к изменению отсутствуют", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                Utility.OpenForm(new Profile.frmProfile(), this);
                return;
            }

            if (string.IsNullOrEmpty(textBox_email.Text) 
                || !(new Regex(@"^[0-9A-Za-z_-]+@[0-9A-Z_a-z]+\.[A-Za-z]+$").IsMatch(textBox_email.Text))
                )
            {
                MessageBox.Show(null, "Email введен некорректно", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (textBox_password.Text != textBox_rePassword.Text)
            {
                MessageBox.Show(null, "Пароли не совпадают", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (Utility.current_user.UserEmail != textBox_email.Text)
                Utility.current_user.UserEmail = textBox_email.Text;

            if (!string.IsNullOrEmpty(textBox_password.Text))
                Utility.current_user.UserPassword = textBox_password.Text;

            if (checkBox_notify.Checked == true)
                Utility.current_user.UserNotify = 1;
            else
                Utility.current_user.UserNotify = 0;

            if (pictureBox.Image != null && ava_change == true)
            {
                using (var ms = new MemoryStream())
                {
                    pictureBox.Image.Save(ms, ImageFormat.Png);

                    ms.Position = 0;

                    Utility.current_user.UserAvatar = ms.ToArray();
                }
            }

            try
            {
                
                Utility.dBase.SaveChanges();

                MessageBox.Show(null, "Изменения успешно сохранены", "Успех", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);

                Utility.OpenForm(new Profile.frmProfile(), this);
            }
            catch
            {
                MessageBox.Show(null, "Произошла ошибка при добавлении", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void button_avaDel_Click(object sender, EventArgs e)
        {
            pictureBox.Image = null;
            pictureBox.BackgroundImage = Properties.Resources.userIcon;
            Utility.current_user.UserAvatar = null;
            ava_change = true;
        }

        private void button_FromCam_Click(object sender, EventArgs e)
        {
            Auth.frmGetPicFromCamera frm = new Auth.frmGetPicFromCamera();

            frm.FormClosing += delegate
            {
                if (frm.bmp != null)
                {
                    pictureBox.Image = frm.bmp;
                    pictureBox.BackgroundImage = null;
                    ava_change = true;
                }
            };

            frm.ShowDialog();
        }
    }
}
