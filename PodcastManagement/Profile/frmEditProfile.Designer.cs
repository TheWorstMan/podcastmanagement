﻿namespace PodcastManagement.Profile
{
    partial class frmEditProfile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_Cancel = new System.Windows.Forms.Button();
            this.button_fromFile = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelLoginCap = new System.Windows.Forms.Label();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.textBox_email = new System.Windows.Forms.TextBox();
            this.textBox_password = new System.Windows.Forms.TextBox();
            this.checkBox_notify = new System.Windows.Forms.CheckBox();
            this.textBox_rePassword = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button_FromCam = new System.Windows.Forms.Button();
            this.button_OK = new System.Windows.Forms.Button();
            this.label_login = new System.Windows.Forms.Label();
            this.button_avaDel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // button_Cancel
            // 
            this.button_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_Cancel.BackColor = System.Drawing.Color.Transparent;
            this.button_Cancel.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(175)))), ((int)(((byte)(211)))));
            this.button_Cancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button_Cancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(217)))), ((int)(((byte)(234)))));
            this.button_Cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Cancel.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Cancel.Location = new System.Drawing.Point(458, 228);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(109, 29);
            this.button_Cancel.TabIndex = 25;
            this.button_Cancel.Text = "Отменить";
            this.button_Cancel.UseVisualStyleBackColor = false;
            this.button_Cancel.Click += new System.EventHandler(this.button_Cancel_Click);
            // 
            // button_fromFile
            // 
            this.button_fromFile.BackColor = System.Drawing.Color.Transparent;
            this.button_fromFile.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(175)))), ((int)(((byte)(211)))));
            this.button_fromFile.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button_fromFile.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(217)))), ((int)(((byte)(234)))));
            this.button_fromFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_fromFile.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_fromFile.Location = new System.Drawing.Point(11, 206);
            this.button_fromFile.Name = "button_fromFile";
            this.button_fromFile.Size = new System.Drawing.Size(90, 25);
            this.button_fromFile.TabIndex = 24;
            this.button_fromFile.Text = "Из файла";
            this.button_fromFile.UseVisualStyleBackColor = false;
            this.button_fromFile.Click += new System.EventHandler(this.button_fromFile_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(214, 153);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 18);
            this.label7.TabIndex = 21;
            this.label7.Text = "Уведомлять:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(214, 91);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 18);
            this.label6.TabIndex = 20;
            this.label6.Text = "Пароль:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(214, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 18);
            this.label2.TabIndex = 16;
            this.label2.Text = "Email:";
            // 
            // labelLoginCap
            // 
            this.labelLoginCap.AutoSize = true;
            this.labelLoginCap.BackColor = System.Drawing.Color.Transparent;
            this.labelLoginCap.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLoginCap.Location = new System.Drawing.Point(214, 23);
            this.labelLoginCap.Name = "labelLoginCap";
            this.labelLoginCap.Size = new System.Drawing.Size(51, 18);
            this.labelLoginCap.TabIndex = 14;
            this.labelLoginCap.Text = "Логин:";
            // 
            // pictureBox
            // 
            this.pictureBox.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox.BackgroundImage = global::PodcastManagement.Properties.Resources.userIcon;
            this.pictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox.Location = new System.Drawing.Point(11, 12);
            this.pictureBox.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(184, 188);
            this.pictureBox.TabIndex = 13;
            this.pictureBox.TabStop = false;
            // 
            // textBox_email
            // 
            this.textBox_email.Font = new System.Drawing.Font("Panton", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_email.Location = new System.Drawing.Point(378, 54);
            this.textBox_email.MaxLength = 50;
            this.textBox_email.Name = "textBox_email";
            this.textBox_email.Size = new System.Drawing.Size(100, 23);
            this.textBox_email.TabIndex = 27;
            // 
            // textBox_password
            // 
            this.textBox_password.Font = new System.Drawing.Font("Panton", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_password.Location = new System.Drawing.Point(378, 90);
            this.textBox_password.MaxLength = 100;
            this.textBox_password.Name = "textBox_password";
            this.textBox_password.PasswordChar = '*';
            this.textBox_password.Size = new System.Drawing.Size(100, 23);
            this.textBox_password.TabIndex = 29;
            // 
            // checkBox_notify
            // 
            this.checkBox_notify.AutoSize = true;
            this.checkBox_notify.Font = new System.Drawing.Font("Panton", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBox_notify.Location = new System.Drawing.Point(378, 157);
            this.checkBox_notify.Name = "checkBox_notify";
            this.checkBox_notify.Size = new System.Drawing.Size(15, 14);
            this.checkBox_notify.TabIndex = 30;
            this.checkBox_notify.UseVisualStyleBackColor = true;
            // 
            // textBox_rePassword
            // 
            this.textBox_rePassword.Font = new System.Drawing.Font("Panton", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_rePassword.Location = new System.Drawing.Point(378, 122);
            this.textBox_rePassword.MaxLength = 100;
            this.textBox_rePassword.Name = "textBox_rePassword";
            this.textBox_rePassword.PasswordChar = '*';
            this.textBox_rePassword.Size = new System.Drawing.Size(100, 23);
            this.textBox_rePassword.TabIndex = 32;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(214, 123);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 18);
            this.label1.TabIndex = 31;
            this.label1.Text = "Повтор пароля:";
            // 
            // button_FromCam
            // 
            this.button_FromCam.BackColor = System.Drawing.Color.Transparent;
            this.button_FromCam.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(175)))), ((int)(((byte)(211)))));
            this.button_FromCam.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button_FromCam.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(217)))), ((int)(((byte)(234)))));
            this.button_FromCam.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_FromCam.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_FromCam.Location = new System.Drawing.Point(105, 206);
            this.button_FromCam.Name = "button_FromCam";
            this.button_FromCam.Size = new System.Drawing.Size(90, 25);
            this.button_FromCam.TabIndex = 33;
            this.button_FromCam.Text = "С камеры";
            this.button_FromCam.UseVisualStyleBackColor = false;
            this.button_FromCam.Click += new System.EventHandler(this.button_FromCam_Click);
            // 
            // button_OK
            // 
            this.button_OK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_OK.BackColor = System.Drawing.Color.Transparent;
            this.button_OK.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(175)))), ((int)(((byte)(211)))));
            this.button_OK.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button_OK.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(175)))), ((int)(((byte)(211)))));
            this.button_OK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_OK.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_OK.Location = new System.Drawing.Point(343, 228);
            this.button_OK.Name = "button_OK";
            this.button_OK.Size = new System.Drawing.Size(109, 29);
            this.button_OK.TabIndex = 34;
            this.button_OK.Text = "Подтверить";
            this.button_OK.UseVisualStyleBackColor = false;
            this.button_OK.Click += new System.EventHandler(this.button_OK_Click);
            // 
            // label_login
            // 
            this.label_login.AutoSize = true;
            this.label_login.BackColor = System.Drawing.Color.Transparent;
            this.label_login.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_login.Location = new System.Drawing.Point(375, 23);
            this.label_login.Name = "label_login";
            this.label_login.Size = new System.Drawing.Size(37, 18);
            this.label_login.TabIndex = 35;
            this.label_login.Text = "neer";
            // 
            // button_avaDel
            // 
            this.button_avaDel.BackColor = System.Drawing.Color.Transparent;
            this.button_avaDel.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(175)))), ((int)(((byte)(211)))));
            this.button_avaDel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button_avaDel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(217)))), ((int)(((byte)(234)))));
            this.button_avaDel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_avaDel.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_avaDel.Location = new System.Drawing.Point(56, 237);
            this.button_avaDel.Name = "button_avaDel";
            this.button_avaDel.Size = new System.Drawing.Size(90, 25);
            this.button_avaDel.TabIndex = 36;
            this.button_avaDel.Text = "Удалить";
            this.button_avaDel.UseVisualStyleBackColor = false;
            this.button_avaDel.Click += new System.EventHandler(this.button_avaDel_Click);
            // 
            // frmEditProfile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::PodcastManagement.Properties.Resources.background;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(579, 272);
            this.Controls.Add(this.button_avaDel);
            this.Controls.Add(this.label_login);
            this.Controls.Add(this.button_OK);
            this.Controls.Add(this.button_FromCam);
            this.Controls.Add(this.textBox_rePassword);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.checkBox_notify);
            this.Controls.Add(this.textBox_password);
            this.Controls.Add(this.textBox_email);
            this.Controls.Add(this.button_Cancel);
            this.Controls.Add(this.button_fromFile);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelLoginCap);
            this.Controls.Add(this.pictureBox);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmEditProfile";
            this.Text = "Редактирование профиля";
            this.Load += new System.EventHandler(this.frmEditProfile_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_Cancel;
        private System.Windows.Forms.Button button_fromFile;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelLoginCap;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.TextBox textBox_email;
        private System.Windows.Forms.TextBox textBox_password;
        private System.Windows.Forms.CheckBox checkBox_notify;
        private System.Windows.Forms.TextBox textBox_rePassword;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_FromCam;
        private System.Windows.Forms.Button button_OK;
        private System.Windows.Forms.Label label_login;
        private System.Windows.Forms.Button button_avaDel;
    }
}