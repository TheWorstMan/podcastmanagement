﻿namespace PodcastManagement.Profile
{
    partial class frmProfile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.labelLoginCap = new System.Windows.Forms.Label();
            this.label_login = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label_regDate = new System.Windows.Forms.Label();
            this.label_email = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label_status = new System.Windows.Forms.Label();
            this.label_notify = new System.Windows.Forms.Label();
            this.button_change = new System.Windows.Forms.Button();
            this.button_back = new System.Windows.Forms.Button();
            this.button_addep = new System.Windows.Forms.Button();
            this.button_admin = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox
            // 
            this.pictureBox.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox.BackgroundImage = global::PodcastManagement.Properties.Resources.userIcon;
            this.pictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox.Location = new System.Drawing.Point(12, 12);
            this.pictureBox.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(184, 188);
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            // 
            // labelLoginCap
            // 
            this.labelLoginCap.AutoSize = true;
            this.labelLoginCap.BackColor = System.Drawing.Color.Transparent;
            this.labelLoginCap.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLoginCap.Location = new System.Drawing.Point(201, 12);
            this.labelLoginCap.Name = "labelLoginCap";
            this.labelLoginCap.Size = new System.Drawing.Size(51, 18);
            this.labelLoginCap.TabIndex = 1;
            this.labelLoginCap.Text = "Логин:";
            // 
            // label_login
            // 
            this.label_login.AutoSize = true;
            this.label_login.BackColor = System.Drawing.Color.Transparent;
            this.label_login.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_login.Location = new System.Drawing.Point(342, 12);
            this.label_login.Name = "label_login";
            this.label_login.Size = new System.Drawing.Size(37, 18);
            this.label_login.TabIndex = 2;
            this.label_login.Text = "neer";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(201, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "Email:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(201, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(135, 18);
            this.label3.TabIndex = 4;
            this.label3.Text = "Дата регистрации:";
            // 
            // label_regDate
            // 
            this.label_regDate.AutoSize = true;
            this.label_regDate.BackColor = System.Drawing.Color.Transparent;
            this.label_regDate.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_regDate.Location = new System.Drawing.Point(342, 68);
            this.label_regDate.Name = "label_regDate";
            this.label_regDate.Size = new System.Drawing.Size(84, 18);
            this.label_regDate.TabIndex = 5;
            this.label_regDate.Text = "19.20.2017";
            // 
            // label_email
            // 
            this.label_email.AutoSize = true;
            this.label_email.BackColor = System.Drawing.Color.Transparent;
            this.label_email.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_email.Location = new System.Drawing.Point(342, 39);
            this.label_email.Name = "label_email";
            this.label_email.Size = new System.Drawing.Size(86, 18);
            this.label_email.TabIndex = 6;
            this.label_email.Text = "asd@asd.ru";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(201, 102);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 18);
            this.label6.TabIndex = 7;
            this.label6.Text = "Статус:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(201, 134);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 18);
            this.label7.TabIndex = 8;
            this.label7.Text = "Уведомлять:";
            // 
            // label_status
            // 
            this.label_status.AutoSize = true;
            this.label_status.BackColor = System.Drawing.Color.Transparent;
            this.label_status.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_status.Location = new System.Drawing.Point(342, 102);
            this.label_status.Name = "label_status";
            this.label_status.Size = new System.Drawing.Size(72, 18);
            this.label_status.TabIndex = 9;
            this.label_status.Text = "Читатель";
            // 
            // label_notify
            // 
            this.label_notify.AutoSize = true;
            this.label_notify.BackColor = System.Drawing.Color.Transparent;
            this.label_notify.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_notify.Location = new System.Drawing.Point(342, 134);
            this.label_notify.Name = "label_notify";
            this.label_notify.Size = new System.Drawing.Size(33, 18);
            this.label_notify.TabIndex = 10;
            this.label_notify.Text = "Нет";
            // 
            // button_change
            // 
            this.button_change.BackColor = System.Drawing.Color.Transparent;
            this.button_change.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(175)))), ((int)(((byte)(211)))));
            this.button_change.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button_change.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(217)))), ((int)(((byte)(234)))));
            this.button_change.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_change.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_change.Location = new System.Drawing.Point(549, 7);
            this.button_change.Name = "button_change";
            this.button_change.Size = new System.Drawing.Size(104, 29);
            this.button_change.TabIndex = 11;
            this.button_change.Text = "Изменить";
            this.button_change.UseVisualStyleBackColor = false;
            this.button_change.Click += new System.EventHandler(this.button_change_Click);
            // 
            // button_back
            // 
            this.button_back.BackColor = System.Drawing.Color.Transparent;
            this.button_back.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(175)))), ((int)(((byte)(211)))));
            this.button_back.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button_back.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(217)))), ((int)(((byte)(234)))));
            this.button_back.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_back.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_back.Location = new System.Drawing.Point(549, 171);
            this.button_back.Name = "button_back";
            this.button_back.Size = new System.Drawing.Size(104, 29);
            this.button_back.TabIndex = 12;
            this.button_back.Text = "На главную";
            this.button_back.UseVisualStyleBackColor = false;
            this.button_back.Click += new System.EventHandler(this.button_back_Click);
            // 
            // button_addep
            // 
            this.button_addep.BackColor = System.Drawing.Color.Transparent;
            this.button_addep.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(175)))), ((int)(((byte)(211)))));
            this.button_addep.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button_addep.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(217)))), ((int)(((byte)(234)))));
            this.button_addep.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_addep.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_addep.Location = new System.Drawing.Point(204, 171);
            this.button_addep.Name = "button_addep";
            this.button_addep.Size = new System.Drawing.Size(141, 29);
            this.button_addep.TabIndex = 13;
            this.button_addep.Text = "Эпизоды";
            this.button_addep.UseVisualStyleBackColor = false;
            this.button_addep.Click += new System.EventHandler(this.button_addep_Click);
            // 
            // button_admin
            // 
            this.button_admin.BackColor = System.Drawing.Color.Transparent;
            this.button_admin.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(175)))), ((int)(((byte)(211)))));
            this.button_admin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button_admin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(217)))), ((int)(((byte)(234)))));
            this.button_admin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_admin.Font = new System.Drawing.Font("Panton", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_admin.Location = new System.Drawing.Point(351, 171);
            this.button_admin.Name = "button_admin";
            this.button_admin.Size = new System.Drawing.Size(163, 29);
            this.button_admin.TabIndex = 14;
            this.button_admin.Text = "Администрирование";
            this.button_admin.UseVisualStyleBackColor = false;
            this.button_admin.Click += new System.EventHandler(this.button_admin_Click);
            // 
            // frmProfile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::PodcastManagement.Properties.Resources.background;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(665, 210);
            this.Controls.Add(this.button_admin);
            this.Controls.Add(this.button_addep);
            this.Controls.Add(this.button_back);
            this.Controls.Add(this.button_change);
            this.Controls.Add(this.label_notify);
            this.Controls.Add(this.label_status);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label_email);
            this.Controls.Add(this.label_regDate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label_login);
            this.Controls.Add(this.labelLoginCap);
            this.Controls.Add(this.pictureBox);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "frmProfile";
            this.Text = "Профиль";
            this.Load += new System.EventHandler(this.frmProfile_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Label labelLoginCap;
        private System.Windows.Forms.Label label_login;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label_regDate;
        private System.Windows.Forms.Label label_email;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label_status;
        private System.Windows.Forms.Label label_notify;
        private System.Windows.Forms.Button button_change;
        private System.Windows.Forms.Button button_back;
        private System.Windows.Forms.Button button_addep;
        private System.Windows.Forms.Button button_admin;
    }
}