﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PodcastManagement.Profile
{
    public partial class frmProfile : Form
    {
        Type backFrm;

        public frmProfile(Type f = null)
        {
            InitializeComponent();
            backFrm = f;
        }

        private void button_back_Click(object sender, EventArgs e)
        {
            Utility.OpenForm(new MainForm(), this);
        }

        private void frmProfile_Load(object sender, EventArgs e)
        {
            label_email.Text = Utility.current_user.UserEmail;
            label_login.Text = Utility.current_user.UserLogin;
            label_regDate.Text = Utility.current_user.UserRegisterDate.ToString();
            label_status.Text = Utility.current_user.UserRole;
            label_notify.Text = Utility.current_user.UserNotify == 1 ? "Да" : "Нет";

            if (Utility.current_user.UserAvatar != null)
            {
                pictureBox.BackgroundImage = null;

                using (var ms = new MemoryStream(Utility.current_user.UserAvatar))
                    pictureBox.Image = Bitmap.FromStream(ms);
            }
            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;

            if (Utility.current_user.UserRole == "Переводчик" || Utility.current_user.UserRole == "Администратор")
                button_addep.Visible = true;
            else
                button_addep.Visible = false;

            if (Utility.current_user.UserRole == "Администратор")
                button_admin.Visible = true;
            else
                button_admin.Visible = false;
        }

        private void button_change_Click(object sender, EventArgs e)
        {
            Utility.OpenForm(new Profile.frmEditProfile(), this);
        }

        private void button_addep_Click(object sender, EventArgs e)
        {
            Utility.OpenForm(new View.frmAddEpisode(typeof(Profile.frmProfile)), this);
        }

        private void button_admin_Click(object sender, EventArgs e)
        {
            Utility.OpenForm(new Admin.frmAdminMenu(), this);
        }
    }
}
