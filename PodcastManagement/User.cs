//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PodcastManagement
{
    using System;
    using System.Collections.Generic;
    
    public partial class User
    {
        public User()
        {
            this.News = new HashSet<News>();
            this.User_has_Translation = new HashSet<User_has_Translation>();
            this.Episode = new HashSet<Episode>();
            this.Podcast = new HashSet<Podcast>();
        }
    
        public int UserId { get; set; }
        public string UserLogin { get; set; }
        public string UserPassword { get; set; }
        public string UserRole { get; set; }
        public byte[] UserAvatar { get; set; }
        public System.DateTime UserRegisterDate { get; set; }
        public string UserEmail { get; set; }
        public sbyte UserNotify { get; set; }
    
        public virtual ICollection<News> News { get; set; }
        public virtual ICollection<User_has_Translation> User_has_Translation { get; set; }
        public virtual ICollection<Episode> Episode { get; set; }
        public virtual ICollection<Podcast> Podcast { get; set; }
    }
}
