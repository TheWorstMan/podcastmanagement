﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PodcastManagement
{
    class MyPicContainer : PictureBox
    {
        public Podcast podcast;

        private bool IsSelected = false;
        public bool Selected
        {
            get
            {
                return IsSelected;
            }
            set
            {
                IsSelected = value;
                this.Invalidate();
            }
        }

        public MyPicContainer(Bitmap bmp, Podcast pod)
        {
            this.Image = bmp;
            this.podcast = pod;
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);

            if(IsSelected)
                pe.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(150, 240, 240, 240)), new Rectangle(new Point(0, 0), this.Size));
        }
    }
}
