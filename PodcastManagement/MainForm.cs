﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PodcastManagement
{
    public partial class MainForm : Form
    {
        private List<MyPicContainer> podcast_pic;
        private Episode lastEpisode;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            var podcasts = Utility.dBase.Podcast;

            podcast_pic = new List<MyPicContainer>();

            foreach(var p in podcasts)
            {
                Bitmap bmp;

                using (var mem = new MemoryStream(p.PodcastLogo))
                {
                    bmp = new Bitmap(mem);
                }

                MyPicContainer pb = new MyPicContainer(bmp, p);

                //PictureBox pb = new PictureBox();
                //pb.Image = bmp;
                pb.SizeMode = PictureBoxSizeMode.Zoom;
                pb.Width = 120;
                pb.Height = 120;
                pb.Click += Pb_Click;

                podcast_pic.Add(pb);
                //pb.Width = flowLayoutPanel_podcast.Width;
                //pb.Height = 0;

                this.flowLayoutPanel_podcast.Controls.Add(pb);
            }

            Pb_Click(podcast_pic.First(), null);     
            
            if(Utility.current_user != null)
            {
                this.flowLayoutPanel_auth.Controls.Clear();

                foreach (var c in Utility.getUProfile(this))
                    this.flowLayoutPanel_auth.Controls.Add(c);
            }

            foreach (var n in Utility.getNews(3))
                this.flowLayoutPanel_news.Controls.Add(n);
                       
        }

        private void Pb_Click(object sender, EventArgs e)
        {
            foreach (var p in podcast_pic)
                p.Selected = false;


            MyPicContainer pic = sender as MyPicContainer;

            pic.Selected = true;

            Episode episode = null;

            try
            {
                label_title.Text = "";
                label_content.Text = "";
                label_time.Text = "";

                foreach(var s in pic.podcast.Season.OrderByDescending(o => o.SeasonStartDate))
                {
                    if (s.Episode.Count <= 0)
                        continue;

                    var epi = s.Episode
                        .Where(ep => ep.TranslationId != null && ep.Translation.TranslationRussian != null)
                        .OrderByDescending(ep => ep.EpisodeDate);

                    if (epi.Count() <= 0)
                        continue;

                    episode = epi.First();

                    break;
                }

                label_title.Text = episode.EpisodeRussianName;
                label_content.Text = episode.Translation.TranslationRussian;
                label_time.Text = episode.Translation.TranslationEndDate.Value.ToString("yyyy-MM-dd");

                lastEpisode = episode;
            }
            catch { }
        }

        private void MainForm_Paint(object sender, PaintEventArgs e)
        {
            //e.Graphics.DrawRectangle(new Pen(new SolidBrush(Color.FromArgb(154, 86, 152)), 4), new Rectangle(1, 1, this.Width - 2, this.Height - 2));
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            Utility.CloseConnection();
            base.OnFormClosing(e);
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            if (!Utility.Auth(textBoxLogin.Text, textBoxPassword.Text))
            {
                MessageBox.Show(null, "Логин или пароль несовпадают", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            this.groupBox_login.Text = "Личный Кабинет";
            this.flowLayoutPanel_auth.Controls.Clear();

            foreach (var c in Utility.getUProfile(this))
                this.flowLayoutPanel_auth.Controls.Add(c);
        }

        private void buttonRegistration_Click(object sender, EventArgs e)
        {
            Utility.OpenForm(new Auth.frmRegistration(this), this, false);
        }

        private void button_navigation_Click(object sender, EventArgs e)
        {
            Utility.OpenForm(new View.frmNavigation(), this);
        }

        private void button_news_Click(object sender, EventArgs e)
        {
            Utility.OpenForm(new View.frmNews(), this);
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Utility.OpenForm(new View.frmEpisodes(podcast_pic.Where(p => p.Selected == true).First().podcast, lastEpisode), this);
        }
    }
}
